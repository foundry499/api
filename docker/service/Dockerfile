# Prepare a base image (copy source, fetch dependencies)
FROM golang:1.14 as base
WORKDIR /go/src/gitlab.com/foundry499/api
COPY . .
RUN go get -d ./...

# Development target (add hot-reloading)
FROM base as development
RUN go get github.com/githubnemo/CompileDaemon
CMD CompileDaemon -log-prefix=false -build="go build ./cmd/service/" -command="./service"

# Build a static binary for the production target
FROM base as builder
ENV CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64
RUN go build ./cmd/service

# Production target (minimal build)
FROM golang:1.14 as production
COPY --from=builder /go/src/gitlab.com/foundry499/api/migrations ./migrations
COPY --from=builder /go/src/gitlab.com/foundry499/api/service .
ENTRYPOINT [ "/go/service" ]