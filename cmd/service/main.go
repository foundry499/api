package main

import (
	"fmt"
	"time"

	"github.com/kelseyhightower/envconfig"
	"github.com/rs/zerolog/log"
	"gitlab.com/foundry499/api/pkg/azure"
	"gitlab.com/foundry499/api/pkg/job"
	"gitlab.com/foundry499/api/pkg/postgres"
	"gitlab.com/foundry499/api/pkg/sshtunnel"
)

var version = "0.0.3"

const (
	pollInterval = 60 * time.Second
)

func main() {
	// Handle command line arguments here
	fmt.Println("version ==> ", version)
	fmt.Println("WARNING: Azure resources will be created and may not be removed. You will be billed for created resources.")

	log.Logger = log.With().Caller().Logger()

	// Import Azure and SSH configuration from environment
	var azureConf azure.Azure
	err := envconfig.Process("AZURE", &azureConf)
	if err != nil {
		log.Fatal().Err(err).Send()
	}
	var sshConf sshtunnel.SSHConfig
	err = envconfig.Process("SSH", &sshConf)
	if err != nil {
		log.Fatal().Err(err).Send()
	}

	//=====================//
	// Connect to Database //
	//=====================//

	// import configs
	var dbConf postgres.DatabaseConfig
	err = envconfig.Process("DB", &dbConf)
	if err != nil {
		log.Fatal().Err(err).Send()
	}

	// connect to db
	db, err := postgres.Init(&dbConf)
	if err != nil {
		log.Fatal().Err(err).Send()
	}
	defer db.Close()

	// job access service
	js := &postgres.JobService{DB: db}

	// vm access service
	vs := &postgres.VirtualMachineService{DB: db}

	//================================//
	// Start long running Job Manager //
	//================================//

	jm := job.NewManager(js, vs, &azureConf, &sshConf)
	jm.Run(pollInterval)
}
