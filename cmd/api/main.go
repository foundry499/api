package main

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
	"github.com/rs/zerolog/log"
	"gitlab.com/foundry499/api/pkg/http"
	"gitlab.com/foundry499/api/pkg/postgres"
)

var version = "0.0.1"

func main() {
	// Handle command line arguments here
	fmt.Println("version ==> ", version)

	// import configs
	var dbConf postgres.DatabaseConfig
	err := envconfig.Process("DB", &dbConf)
	if err != nil {
		log.Fatal().Msg("Could not access environment vars")
	}

	// connect to db
	db, err := postgres.Init(&dbConf)
	if err != nil {
		log.Fatal().Err(err).Send()
	}
	defer db.Close()

	// db interface services
	js := &postgres.JobService{DB: db}
	us := &postgres.UserService{DB: db}
	vms := &postgres.VirtualMachineService{DB: db}

	s := http.Services{JobService: js, UserService: us, VMService: vms}

	http.Start(8080, &s)
}
