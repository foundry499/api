# Foundry API

A RESTful API service for the Foundry499 project.

## Pre-requisites

1. You need to be on a Linux-based system (Windows 10 works via WSL). We've tested Fedora, Fedora in WSL, and Ubuntu in WSL.
2. You need to have GNU Make installed. Check with `make -v`.
3. You need to have _either_ Docker or Podman installed. If Podman is available it will automatically be used instead of Docker.
4. To start the development environment you also need _either_ Docker Compose or Podman Compose installed.
5. [Go](https://golang.org/doc/install) must be installed on your system and the GOPATH must be set correctly. 

## Usage

| Command           | Purpose                                                                                                                                  |
| ----------------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| `make`            | Build the development API image and tag it as `foundry499/api:development`                                                               |
| `make up`         | Start the API and ServiceQueue with hot-reloading enabled, and start a local PostgreSQL database                                         |
| `make logs`       | Display logs from the running API, ServiceQueue, and database container                                                                  |
| `make down`       | Stop the API, ServiceQueue, and PostgreSQL database.                                                                                     |
| `make clean`      | Stop the API then delete stopped containers, unused volumes, and untagged images (incl. build cache)                                     |
| `make test`       | Runs tests in a container                                                                                                                |
| `make production` | Build minimal production API and ServiceQueue containers and tag them as `foundry499/api:production` and `foundry499/service:production` |

Once the API is running (via `make up`), you can access it at `http://localhost:8080`.

### Recommended Developer Usage

1. Run `go get ./...` to download dependencies
1. Build the containers: `make`
1. Start the development server: `make up`
1. View log output: `make logs`

The API should be externally accessible at `localhost:8080`. Use with caution! The test ServiceQueue is capable of interacting with Azure and you **will** be charged by Microsoft if you start new jobs.

## API Documentation

Documentation is auto-generated from comments in `pkg/http` by Swagger. The documentation can be viewed with swagger-ui at `http://localhost:8080/docs/index.html` or at whatever address the the API is hosted at.

The OpenAPI Standard (OAS) documentation is located at `url/docs/doc.json`. Any OAS-compatible documentation viewer can make use of it to display the documentation in your preferred format. Our deployment uses [Redoc](https://github.com/Redocly/redoc), located at [foundry499.com/docs](https://www.foundry499.com/docs), which we would highly recommend.
