tool = $(shell if type podman >/dev/null 2>&1; then echo "podman"; else echo "docker"; fi)

.PHONY: default production development up logs down test clean

default: development

production:
	@echo "============= Building API image (target: production) =============="
	$(tool) build -f docker/api/Dockerfile -t foundry499/api/api:production .
	$(tool) build -f docker/service/Dockerfile -t foundry499/api/service:production .

development:
	@echo "============= Building API image (target: development) ============="
	$(tool) build --target=development -f docker/api/Dockerfile -t foundry499/api/api:development .
	$(tool) build --target=development -f docker/service/Dockerfile -t foundry499/api/service:development .
	go get github.com/swaggo/swag/cmd/swag

up:
	@echo "======================= Starting API locally ======================="
	swag init -g ./pkg/http/*
	$(tool)-compose up -d


logs:
ifeq ($(tool), docker)
	docker-compose logs -f
else
	@echo "================ Displaying logs for API container ================"
	podman logs -f api_api_1
	@echo "============== Displaying logs for Service container =============="
	podman logs -f api_service_1
endif

down:
	@echo "======================== Stopping local API ========================"
	$(tool)-compose down

test:
	$(tool) run foundry499/api:development go test -v -cover ./...

clean: down

clean:
	@echo "=========================== Cleaning up ============================"
	rm -f ./api
	$(tool) rmi foundry499/api/api:development
	$(tool) rmi foundry499/api/api:production
	$(tool) rmi foundry499/api/service:development
	$(tool) rmi foundry499/api/service:production
	$(tool) system prune
	$(tool) volume prune
ifeq ($(tool), podman)
	podman pod rm -f api
	podman pod prune
endif

docs: 
	@echo "====================== Building docs for API ======================="
