BEGIN;

INSERT INTO Users (full_name, email, password_hash) 
SELECT 'Admin', 'admin@foundry499.com', '$2y$12$AD9urpwvB0ga/tlpTlMHCOyw9ORWnt5k8/yb9NLcUwaPhuGLN7pru' -- password is "letmein"
WHERE
    NOT EXISTS (
        SELECT Email FROM Users WHERE Email = 'admin@foundry499.com'
    );

COMMIT;
