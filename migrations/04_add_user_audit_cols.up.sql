BEGIN;

ALTER TABLE users
    ADD COLUMN created_by INT;
ALTER TABLE users
    ADD COLUMN created_at TIMESTAMP DEFAULT NOW();
ALTER TABLE users
    ADD COLUMN updated_by INT DEFAULT NULL;
ALTER TABLE users
    ADD COLUMN updated_at TIMESTAMP DEFAULT NULL;
ALTER TABLE users
    ADD COLUMN is_deleted BOOLEAN DEFAULT FALSE NOT NULL;

UPDATE users
    SET created_by = 1; -- probably admin account
UPDATE users
    SET updated_by = 1; 

ALTER TABLE users 
    ALTER COLUMN created_at SET NOT NULL;
ALTER TABLE users 
    ALTER COLUMN created_by SET NOT NULL;


COMMIT;