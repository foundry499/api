BEGIN;

ALTER TABLE users 
    DROP COLUMN created_by;
ALTER TABLE users 
    DROP COLUMN created_at;
ALTER TABLE users 
    DROP COLUMN updated_by;
ALTER TABLE users 
    DROP COLUMN updated_at;
ALTER TABLE users 
    DROP COLUMN is_deleted;

COMMIT;