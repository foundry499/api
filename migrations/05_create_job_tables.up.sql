BEGIN;

CREATE TABLE Jobs (
    id INT UNIQUE GENERATED ALWAYS AS IDENTITY,
    job_name VARCHAR(256),
    tags JSON,
    docker_image VARCHAR(256) NOT NULL,
    start_command VARCHAR(256) NOT NULL,
    env_vars JSON,
    vm_type INT NOT NULL,
    storage INT NOT NULL,
    cores INT NOT NULL,
    memory INT NOT NULL,
    created_by INT REFERENCES Users(id) NOT NULL,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_by INT REFERENCES Users(id) DEFAULT NULL,
    updated_at TIMESTAMP DEFAULT NULL,
    is_deleted BOOLEAN DEFAULT FALSE NOT NULL
);

CREATE TABLE Job_Status (
    id INT UNIQUE GENERATED ALWAYS AS IDENTITY,
    job_id INT REFERENCES Jobs(id),
    status_code INT NOT NULL,
    created_at TIMESTAMP DEFAULT NOW()
);

COMMIT;