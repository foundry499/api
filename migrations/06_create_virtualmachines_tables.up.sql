BEGIN;

CREATE TABLE VirtualMachines (
    id INT UNIQUE GENERATED ALWAYS AS IDENTITY,
    vm_name VARCHAR(256) NOT NULL,
    vm_type INT NOT NULL,
    cores INT NOT NULL,
    memory INT NOT NULL,
    storage INT NOT NULL,
    estimated_cost DECIMAL NOT NULL,
    ip_address VARCHAR(64) DEFAULT NULL,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NULL
);

CREATE TABLE VirtualMachine_Status (
    id INT UNIQUE GENERATED ALWAYS AS IDENTITY,
    virtualmachine_id INT REFERENCES VirtualMachines(id),
    status_code INT NOT NULL,
    created_at TIMESTAMP DEFAULT NOW()
);

COMMIT;