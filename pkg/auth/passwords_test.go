package auth

import (
	"fmt"
	"testing"
)

func TestEasyPasswordHash(t *testing.T) {
	p := "mypassword"
	hp, err := HashPassword(p)
	if err != nil {
		t.Fatalf("HashPassword error: %s", err)
	}
	fmt.Print(hp)
	if CheckPasswordHash(p, hp) != true {
		t.Errorf("%v should hash %s correctly", hp, p)
	}

	bp := "someotherpass"
	if CheckPasswordHash(bp, hp) != false {
		t.Errorf("%v and %s should not match", hp, bp)
	}
}

func TestHardPasswordHash(t *testing.T) {
	p := "MyP@$$w_0rd"
	hp, err := HashPassword(p)
	if err != nil {
		t.Fatalf("HashPassword error: %s", err)
	}
	fmt.Print(hp)
	if CheckPasswordHash(p, hp) != true {
		t.Errorf("%v should hash %s correctly", hp, p)
	}

	bp := "someotherpass"
	if CheckPasswordHash(bp, hp) != false {
		t.Errorf("%v and %s should not match", hp, bp)
	}
}

func TestShortPasswordHash(t *testing.T) {
	p := "pw"
	hp, err := HashPassword(p)
	if err != nil {
		t.Fatalf("HashPassword error: %s", err)
	}
	fmt.Print(hp)
	if CheckPasswordHash(p, hp) != true {
		t.Errorf("%v should hash %s correctly", hp, p)
	}

	bp := "someotherpass"
	if CheckPasswordHash(bp, hp) != false {
		t.Errorf("%v and %s should not match", hp, bp)
	}
}

func TestLongPasswordHash(t *testing.T) {
	p := "PassWordPassWordPassWordPassWordPassWordPassWordPassWordPassWord" // 64 characters
	hp, err := HashPassword(p)
	if err != nil {
		t.Fatalf("HashPassword error: %s", err)
	}
	fmt.Print(hp)
	if CheckPasswordHash(p, hp) != true {
		t.Errorf("%v should hash %s correctly", hp, p)
	}

	bp := "someotherpass"
	if CheckPasswordHash(bp, hp) != false {
		t.Errorf("%v and %s should not match", hp, bp)
	}
}

func TestEmojiPasswordHash(t *testing.T) {
	p := "🔑🤣🚀" // unbelievable that this works
	hp, err := HashPassword(p)
	if err != nil {
		t.Fatalf("HashPassword error: %s", err)
	}
	fmt.Print(hp)
	if CheckPasswordHash(p, hp) != true {
		t.Errorf("%v should hash %s correctly", hp, p)
	}

	bp := "someotherpass"
	if CheckPasswordHash(bp, hp) != false {
		t.Errorf("%v and %s should not match", hp, bp)
	}
}
