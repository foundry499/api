// Source: elliotchance's sshtunnel (https://github.com/elliotchance/sshtunnel, MIT License, 52c7403b)
// Modified to accept Unix sockets and use zerolog

package sshtunnel

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/rs/zerolog/log"
)

// Endpoint represents a TCP server or Unix socket
type Endpoint struct {
	Type string // "tcp" or "unix"
	Host string // tcp
	Port int    // tcp
	User string // tcp
	Path string // unix socket
}

// NewEndpoint instantiates a new Endpoint
func NewEndpoint(s string) *Endpoint {
	endpoint := &Endpoint{
		Type: "tcp",
	}

	if parts := strings.Split(s, "://"); len(parts) > 1 {
		endpoint.Type = parts[0]
		s = parts[1]
	}

	switch endpoint.Type {
	case "tcp":
		if parts := strings.Split(s, "@"); len(parts) > 1 {
			endpoint.User = parts[0]
			endpoint.Host = parts[1]
		}

		if parts := strings.Split(endpoint.Host, ":"); len(parts) > 1 {
			endpoint.Host = parts[0]
			endpoint.Port, _ = strconv.Atoi(parts[1])
		}
	case "unix":
		endpoint.Path = s
	default:
		log.Panic().Msgf("Invalid endpoint type: %s", endpoint.Type)
	}

	return endpoint
}

func (endpoint *Endpoint) String() string {
	switch endpoint.Type {
	case "tcp":
		return fmt.Sprintf("%s:%d", endpoint.Host, endpoint.Port)
	case "unix":
		return endpoint.Path
	default:
		log.Panic().Msgf("Invalid endpoint type: %s", endpoint.Type)
	}
	return ""
}
