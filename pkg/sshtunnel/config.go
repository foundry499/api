package sshtunnel

// SSHConfig holds configuration values for SSH tunnels to the Worker VMs
type SSHConfig struct {
	User       string `envconfig:"SSH_USER" required:"true"`
	PrivateKey string `envconfig:"SSH_PRIVATE_KEY" required:"true"`
	Socket     string `envconfig:"SSH_SOCKET" required:"true"`
}
