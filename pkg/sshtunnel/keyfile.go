package sshtunnel

import (
	"io/ioutil"
	"strings"

	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/ssh"
)

// PublicKeyFromString parses an SSH private key from the provided string
// so it can be used to authenticate with a server
func PublicKeyFromString(privateKey string) ssh.AuthMethod {

	// Replace \n with actual newlines (if the string does not contain '\n' this does not modify it).
	// Input is typically read from an envvar, so this allows keys to be passed as multiline strings if supported,
	// or as a single-line string separated with "\n" anywhere multiline environment variables are not supported.
	input := strings.ReplaceAll(privateKey, "\\n", "\n")

	// Parse the keyfile
	key, err := ssh.ParsePrivateKey([]byte(input))
	if err != nil {
		log.Panic().Err(err).Msg("failed to parse keyfile")
	}
	return ssh.PublicKeys(key)
}

// PublicKeyFromFile parses an SSH private key from the provided file
// so it can be used to authenticate with a server
func PublicKeyFromFile(file string) ssh.AuthMethod {
	buffer, err := ioutil.ReadFile(file)
	if err != nil {
		log.Panic().Err(err).Msgf("keyfile not found: %s", file)
	}

	key, err := ssh.ParsePrivateKey(buffer)
	if err != nil {
		log.Panic().Err(err).Msg("failed to parse keyfile")
	}
	return ssh.PublicKeys(key)
}
