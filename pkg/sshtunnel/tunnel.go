// Source: elliotchance's sshtunnel (https://github.com/elliotchance/sshtunnel, MIT License, 52c7403b)
// Modified to accept Unix sockets and use zerolog

package sshtunnel

import (
	"io"
	"net"
	"sync"

	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/ssh"
)

// SSHTunnel represents three endpoints used to create a tunnel
// Connections go from Local to Remote via Server
type SSHTunnel struct {
	Local  *Endpoint
	Server *Endpoint
	Remote *Endpoint
	Config *ssh.ClientConfig
	close  chan interface{}
	closed bool
}

// Start opens a connection on the configured SSHTunnel.
// The caller should call Close() when finished.
func (tunnel *SSHTunnel) Start() error {
	listener, err := net.Listen(tunnel.Local.Type, tunnel.Local.String())
	if err != nil {
		log.Error().Err(err).Msg("sshtunnel: failed to start tunnel")
		return err
	}
	tunnel.Local.Port = listener.Addr().(*net.TCPAddr).Port
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Error().Err(err).Msg("sshtunnel: failed to accept connection")
			return err
		}
		log.Debug().Msg("sshtunnel: accepted connection")
		var wg sync.WaitGroup
		wg.Add(1)
		go tunnel.forward(conn, &wg)
		wg.Wait()
		log.Debug().Msg("sshtunnel: tunnel closed")
		break
	}
	err = listener.Close()
	if err != nil {
		log.Error().Err(err).Msg("sshtunnel: failed to close listener")
		return err
	}
	return nil
}

func (tunnel *SSHTunnel) forward(localConn net.Conn, wg *sync.WaitGroup) {
	serverConn, err := ssh.Dial(tunnel.Server.Type, tunnel.Server.String(), tunnel.Config)
	if err != nil {
		log.Error().Err(err).Msgf("sshtunnel: server dial error (%s)", tunnel.Server.String())
		return
	}
	log.Debug().Msgf("sshtunnel: connected to %s (1 of 2)", tunnel.Server.String())
	remoteConn, err := serverConn.Dial(tunnel.Remote.Type, tunnel.Remote.String())
	if err != nil {
		log.Error().Err(err).Msgf("sshtunnel: remote dial error (%s)", tunnel.Remote.String())
		return
	}
	log.Debug().Msgf("sshtunnel: connected to %s (2 of 2)", tunnel.Remote.String())
	copyConn := func(writer, reader net.Conn) {
		_, err := io.Copy(writer, reader)
		if err != nil && !tunnel.closed {
			log.Error().Err(err).Msg("sshtunnel: io.Copy error")
		}
	}
	go copyConn(localConn, remoteConn)
	go copyConn(remoteConn, localConn)
	<-tunnel.close
	log.Debug().Msg("sshtunnel: close signal received, closing...")
	tunnel.closed = true
	_ = localConn.Close()
	_ = serverConn.Close()
	_ = remoteConn.Close()
	wg.Done()
	return
}

// Close terminates the SSH tunnel, freeing network resources and preventing further communication.
func (tunnel *SSHTunnel) Close() {
	tunnel.close <- struct{}{}
	return
}

// NewSSHTunnel creates a new single-use tunnel. Supplying "0" for localport will use a random port.
func NewSSHTunnel(tunnel string, auth ssh.AuthMethod, destination string, localport string) *SSHTunnel {

	localEndpoint := NewEndpoint("tcp://localhost:" + localport)

	server := NewEndpoint(tunnel)
	if server.Port == 0 {
		server.Port = 22
	}

	sshTunnel := &SSHTunnel{
		Config: &ssh.ClientConfig{
			User: server.User,
			Auth: []ssh.AuthMethod{auth},
			HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) error {
				// Always accept key.
				return nil
			},
		},
		Local:  localEndpoint,
		Server: server,
		Remote: NewEndpoint(destination),
		close:  make(chan interface{}),
	}

	return sshTunnel
}
