package rand

import (
	"math/rand"
	"time"
)

// RandomHexString generates a random string (with the provided length) made up of hexidecimal digits
func RandomHexString(length int) string {
	letters := []rune("0123456789abcdef")

	rand.Seed(time.Now().UnixNano()) // just reseed every time for now

	b := make([]rune, length)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
