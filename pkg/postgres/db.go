package postgres

import (
	"context"
	"fmt"
	"net/url"
	"time"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file" // Used to read migrations from local filesystem
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // database driver for postgres
	"github.com/rs/zerolog/log"
)

// DatabaseConfig stores connection details for a database
type DatabaseConfig struct {
	Host     string `envconfig:"DB_HOST"`
	Port     string `envconfig:"DB_PORT"`
	User     string `envconfig:"DB_USER"`
	Password string `envconfig:"DB_PASSWORD"`
	Name     string `envconfig:"DB_NAME"`
}

// Init opens a database connection and applies required migrations
func Init(dbConf *DatabaseConfig) (*sqlx.DB, error) {

	// Build a connection string
	connection := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", // TODO: enable sslmode
		url.PathEscape(dbConf.Host),
		url.PathEscape(dbConf.Port),
		url.PathEscape(dbConf.User),
		url.PathEscape(dbConf.Password),
		url.PathEscape(dbConf.Name),
	)

	db, err := connect(connection)
	if err != nil {
		return nil, err
	}

	err = runMigrations(db)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func connect(connection string) (*sqlx.DB, error) {
	db, err := sqlx.Open("postgres", connection)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	err = db.PingContext(ctx)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func runMigrations(db *sqlx.DB) error {
	driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	if err != nil {
		return err
	}

	log.Debug().Msg("Starting database migrations")
	defer log.Debug().Msg("Finished database migrations")

	m, err := migrate.NewWithDatabaseInstance(
		"file://migrations",
		"postgres",
		driver,
	)
	if err != nil {
		log.Error().Err(err).Msg("Failed to open the migration files")
		return err
	}

	versionBefore, _, err := m.Version()
	if err != nil && err != migrate.ErrNilVersion {
		return err
	}

	err = m.Up()
	if err != nil && err != migrate.ErrNoChange {
		log.Error().Err(err).Msg("Failed to apply migrations")
		return err
	}

	versionAfter, _, err := m.Version()
	if err != nil {
		return err
	}

	if versionBefore == versionAfter {
		log.Info().Msgf("Database migrations are up to date (v%d)", versionAfter)
	} else {
		log.Info().Msgf("Database migrations applied (v%d to v%d)", versionBefore, versionAfter)
	}
	return nil
}
