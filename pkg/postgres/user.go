package postgres

import (
	"crypto/sha1"
	"encoding/hex"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/foundry499/api/pkg/types"
)

// UserService represents a PostgreSQL implementation of api.UserService.
type UserService struct {
	// contain db connection
	DB *sqlx.DB
}

// GetUserByID returns a user for a given ID.
func (s *UserService) GetUserByID(id int) (*types.User, error) {
	var u types.User
	err := s.DB.Get(&u, "SELECT * FROM users WHERE id=$1 AND is_deleted=FALSE", id)
	return &u, err
}

// GetUserByEmail returns a user for a given ID.
func (s *UserService) GetUserByEmail(email string) (*types.User, error) {
	var u types.User
	err := s.DB.Get(&u, "SELECT * FROM users WHERE email=$1 AND is_deleted=FALSE", email)
	return &u, err
}

// GetAllUsers returns all users
func (s *UserService) GetAllUsers() ([]*types.User, error) {
	var users []*types.User
	err := s.DB.Select(&users, "SELECT * FROM users WHERE is_deleted=FALSE")
	return users, err
}

// CreateUser creates a new user
func (s *UserService) CreateUser(u *types.User, auditID int) (int, error) {
	var id int
	q := `INSERT INTO users (full_name, email, role, password_hash, created_by) VALUES ($1, $2, $3, $4, $5) RETURNING id`
	err := s.DB.QueryRowx(q, u.Name, u.Email, u.Role, u.PasswordHash, auditID).Scan(&id)
	if err != nil {
		return -1, err
	}
	return id, nil
}

// UpdateUserByID creates a new user
func (s *UserService) UpdateUserByID(id int, u *types.User, auditID int) error {
	q := `
	UPDATE users 
	SET full_name=$2, email=$3, role=$4, password_hash=$5, updated_by=$6, updated_at=$7
	WHERE id=$1`
	_, err := s.DB.Exec(q, u.ID, u.Name, u.Email, u.Role, u.PasswordHash, auditID, time.Now())
	return err
}

// DeleteUser doesn't really delete the user - it removes personal details and marks the user removed.DeleteUser
// We don't really want to remove ID's from the database as it will break references.
func (s *UserService) DeleteUser(id int, auditID int) error {
	u, err := s.GetUserByID(id)
	newName := "Name Deleted"
	// hash the email field so it's unique and unrecoverable
	email := u.Email + time.Now().String()
	h := sha1.New()
	h.Write([]byte(email))
	hs := hex.EncodeToString(h.Sum(nil))

	q := `
	UPDATE users 
	SET is_deleted=TRUE, updated_by=$2, email=$3, full_name=$4
	WHERE id=$1`
	_, err = s.DB.Exec(q, id, auditID, hs, newName)
	return err
}
