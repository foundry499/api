package postgres

import (
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/foundry499/api/pkg/types"
)

// JobService represents a PostgreSQL implementation of api.JobService.
type JobService struct {
	// contain db connection
	DB *sqlx.DB
}

// GetJobByID returns a single job with the matching id
func (s *JobService) GetJobByID(id int) (*types.Job, error) {
	var j types.Job
	err := s.DB.Get(&j,
		`SELECT jobs.*, job_status.status_code FROM jobs 
		INNER JOIN job_status ON jobs.id = job_status.job_id
		WHERE jobs.id=$1
		ORDER BY job_status.created_at DESC`, id)
	if err != nil {
		return nil, err
	}
	return &j, err
}

// GetJobsByUser returns an array of jobs where they match the user
func (s *JobService) GetJobsByUser(id int) ([]*types.Job, error) {
	var j []*types.Job
	err := s.DB.Select(&j,
		`SELECT jobs.*, job_status.status_code FROM jobs 
		INNER JOIN job_status ON jobs.id = job_status.job_id
		WHERE jobs.created_by=$1`, id)
	if err != nil {
		return nil, err
	}
	return j, err
}

// GetAssignedJobsWithReadyVM returns an array of jobs that are ready to deploy.
// This means the jobs are in the "Assigned" state and are assigned to a VM that is in the "Ready" state.
func (s *JobService) GetAssignedJobsWithReadyVM() ([]*types.Job, error) {
	var j []*types.Job
	err := s.DB.Select(&j,
		`SELECT jobs.*, latest_status.status_code FROM jobs
		JOIN (
			SELECT DISTINCT ON (job_id) * FROM job_status
			ORDER BY job_id, created_at DESC
		) AS latest_status
		ON jobs.id = latest_status.job_id
		JOIN (
			SELECT DISTINCT ON (virtualmachine_id) * FROM virtualmachine_status
			ORDER BY virtualmachine_id, created_at DESC
		) AS vm_status
		ON jobs.virtualmachine_id = vm_status.virtualmachine_id
		WHERE vm_status.status_code=$1
		AND latest_status.status_code=$2`, types.Ready, types.Assigned)
	if err != nil {
		return nil, err
	}
	return j, err
}

// GetJobsByCurrentStatus returns an array of jobs currently in the provided status
func (s *JobService) GetJobsByCurrentStatus(status types.JobStatusCode) ([]*types.Job, error) {
	var j []*types.Job
	err := s.DB.Select(&j,
		`SELECT jobs.*, latest_status.status_code FROM jobs
		JOIN (
			SELECT DISTINCT ON (job_id) * FROM job_status
			ORDER BY job_id, created_at DESC
		) AS latest_status
		ON jobs.id = latest_status.job_id
		WHERE latest_status.status_code=$1`, status)
	if err != nil {
		return nil, err
	}
	return j, err
}

// GetJobsByVirtualMachine returns an array of jobs with the matching VM Assignment
func (s *JobService) GetJobsByVirtualMachine(id int) ([]*types.Job, error) {
	var j []*types.Job
	err := s.DB.Select(&j,
		`SELECT jobs.*, latest_status.status_code FROM jobs
		JOIN (
			SELECT DISTINCT ON (job_id) * FROM job_status
			ORDER BY job_id, created_at DESC
		) AS latest_status
		ON jobs.id = latest_status.job_id
		WHERE jobs.virtualmachine_id=$1`, id)
	if err != nil {
		return nil, err
	}
	return j, err
}

// GetAllJobs returns an array of all jobs
func (s *JobService) GetAllJobs() ([]*types.Job, error) {
	var j []*types.Job
	err := s.DB.Select(&j,
		`SELECT jobs.*, latest_status.status_code FROM jobs
		JOIN (
			SELECT DISTINCT ON (job_id) * FROM job_status
			ORDER BY job_id, created_at DESC
		) AS latest_status
		ON jobs.id = latest_status.job_id WHERE is_deleted=false`)
	if err != nil {
		return nil, err
	}
	return j, err
}

// CreateJob creates a new job, and creates an initial job status
func (s *JobService) CreateJob(j *types.Job, auditID int) (int, error) {
	var id int
	q := `
	INSERT INTO jobs 
	(job_name, tags, docker_image, start_command, env_vars, vm_type, storage, cores, memory, created_by) 
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING id`
	err := s.DB.QueryRowx(q, j.Name, j.Tags, j.DockerImage, j.StartCommand, j.EnvVars, j.VMType, j.Storage, j.Cores, j.Memory, auditID).Scan(&id)

	if err != nil {
		return -1, err
	}
	return id, nil
}

// DeleteJob removes a job from the db
func (s *JobService) DeleteJob(id int, auditID int) error {
	q := `
	UPDATE jobs 
	SET is_deleted=TRUE, updated_by=$2
	WHERE id=$1`
	_, err := s.DB.Exec(q, id, auditID)
	return err
}

// UpdateJobByID updates jobs and updates the audit tags
func (s *JobService) UpdateJobByID(id int, u *types.Job, auditID int) error {
	q := `
	UPDATE jobs 
	SET job_name=$2, tags=$3, virtualmachine_id=$4, container_id=$5, logs_stdout=$6, logs_stderr=$7, updated_by=$8, updated_at=$9
	WHERE id=$1`
	_, err := s.DB.Exec(q, u.ID, u.Name, u.Tags, u.VirtualMachineID, u.ContainerID, u.LogsStdout, u.LogsStderr, auditID, time.Now())
	return err
}

// CreateStatus updates jobs and updates the audit tags
func (s *JobService) CreateStatus(js *types.JobStatus) (int, error) {
	var id int
	q := `INSERT INTO job_status (job_id, status_code) VALUES ($1, $2) RETURNING id`
	err := s.DB.QueryRowx(q, js.JobID, js.StatusCode).Scan(&id)
	if err != nil {
		return -1, err
	}
	return id, nil
}

// GetStatusesByJobID returns an array of JobStatus associated with the given job
func (s *JobService) GetStatusesByJobID(id int) ([]*types.JobStatus, error) {
	var js []*types.JobStatus
	err := s.DB.Select(&js,
		`SELECT * FROM job_status 
		WHERE job_id = $1`, id)
	if err != nil {
		return nil, err
	}
	return js, err
}

// GetAllJobStatuses returns an array of JobStatus
func (s *JobService) GetAllJobStatuses() ([]*types.JobStatus, error) {
	var js []*types.JobStatus
	err := s.DB.Select(&js, `SELECT * FROM job_status`)
	if err != nil {
		return nil, err
	}
	return js, err
}
