package postgres

import (
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/foundry499/api/pkg/types"
)

// VirtualMachineService represents a PostgreSQL implementation of api.VirtualMachine.
type VirtualMachineService struct {
	// contain db connection
	DB *sqlx.DB
}

// GetVMByID returns a single VM with the matching id
func (s *VirtualMachineService) GetVMByID(id int) (*types.VirtualMachine, error) {
	var v types.VirtualMachine
	err := s.DB.Get(&v,
		`SELECT virtualmachines.*, virtualmachine_status.status_code FROM virtualmachines 
		INNER JOIN virtualmachine_status ON virtualmachines.id = virtualmachine_status.virtualmachine_id
		WHERE virtualmachines.id=$1
		ORDER BY virtualmachine_status.created_at DESC`, id)
	if err != nil {
		return nil, err
	}
	return &v, err
}

// CreateVM creates a VM with a default Provisioning status
func (s *VirtualMachineService) CreateVM(v *types.VirtualMachine) (int, error) {
	var id int
	q := `
	INSERT INTO virtualmachines 
	(vm_name, cores, memory, storage, estimated_cost, vm_type)
	VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`
	err := s.DB.QueryRowx(q, v.Name, v.Cores, v.Memory, v.Storage, v.EstimatedCost, v.VMType).Scan(&id)
	if err != nil {
		return -1, err
	}

	s.CreateStatus(&types.VMStatus{VirtualMachineID: id, StatusCode: types.Provisioning})

	return id, nil
}

// UpdateVMByID updates a VM
func (s *VirtualMachineService) UpdateVMByID(id int, u *types.VirtualMachine) error {
	q := `
	UPDATE virtualmachines
	SET estimated_cost=$2, ip_address=$3, updated_at=$4
	WHERE id=$1`
	_, err := s.DB.Exec(q, u.ID, u.EstimatedCost, u.IPAddress, time.Now())
	return err
}

// CreateStatus creates a new status for a VM
func (s *VirtualMachineService) CreateStatus(v *types.VMStatus) (int, error) {
	var id int
	q := `
	INSERT INTO virtualmachine_status
	(virtualmachine_id, status_code)
	VALUES ($1, $2) RETURNING id`
	err := s.DB.QueryRowx(q, v.VirtualMachineID, v.StatusCode).Scan(&id)
	if err != nil {
		return -1, err
	}

	return id, nil
}

// GetVMs returns an array of VMs
func (s *VirtualMachineService) GetVMs() ([]*types.VirtualMachine, error) {
	var vms []*types.VirtualMachine
	err := s.DB.Select(&vms, `SELECT * FROM virtualmachines`)
	if err != nil {
		return nil, err
	}
	return vms, err
}

// GetReadyVMsWithNoActiveJobs returns VMs in the Ready state that have no unfinished jobs assigned to them
func (s *VirtualMachineService) GetReadyVMsWithNoActiveJobs() ([]*types.VirtualMachine, error) {
	var vms []*types.VirtualMachine
	err := s.DB.Select(&vms,
		`SELECT virtualmachines.*, latest_status.status_code FROM virtualmachines
		JOIN (
			SELECT DISTINCT ON (virtualmachine_id) * FROM virtualmachine_status
			ORDER BY virtualmachine_id, created_at DESC
		) AS latest_status
		ON virtualmachines.id = latest_status.virtualmachine_id
		WHERE virtualmachines.id NOT IN (
			SELECT jobs.virtualmachine_id FROM jobs
			JOIN (
				SELECT DISTINCT ON (job_id) * FROM job_status
				ORDER BY job_id, created_at DESC
			) AS js
			ON js.job_id = jobs.id
			WHERE js.status_code<=$1
		) AND latest_status.status_code=$2`, types.Running, types.Ready)
	if err != nil {
		return nil, err
	}
	return vms, err
}

// GetVMsByCurrentStatus returns an array of VMs currently in the provided status
func (s *VirtualMachineService) GetVMsByCurrentStatus(status types.VMStatusCode) ([]*types.VirtualMachine, error) {
	var vms []*types.VirtualMachine
	err := s.DB.Select(&vms,
		`SELECT virtualmachines.*, latest_status.status_code FROM virtualmachines
		JOIN (
			SELECT DISTINCT ON (virtualmachine_id) * FROM virtualmachine_status
			ORDER BY virtualmachine_id, created_at DESC
		) AS latest_status
		ON virtualmachines.id = latest_status.virtualmachine_id
		WHERE latest_status.status_code=$1`, status)
	if err != nil {
		return nil, err
	}
	return vms, err
}

// GetVMStatuses returns an array of JobStatus
func (s *VirtualMachineService) GetVMStatuses() ([]*types.VMStatus, error) {
	var vms []*types.VMStatus
	err := s.DB.Select(&vms, `SELECT * FROM virtualmachine_status`)
	if err != nil {
		return nil, err
	}
	return vms, err
}
