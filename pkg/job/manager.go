package job

import (
	"fmt"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/foundry499/api/pkg/azure"
	"gitlab.com/foundry499/api/pkg/postgres"
	"gitlab.com/foundry499/api/pkg/rand"
	"gitlab.com/foundry499/api/pkg/sshtunnel"
	"gitlab.com/foundry499/api/pkg/types"
)

const (
	// StuckResourceTimeout controls how long to wait before deciding a Job or VM is stuck (must be several times longer than polling interval)
	StuckResourceTimeout = time.Duration(10 * time.Minute)
)

// Manager implements the main logic for processing submitted jobs
type Manager struct {
	jobSvc *postgres.JobService
	vmSvc  *postgres.VirtualMachineService
	az     *azure.Azure
	ssh    *sshtunnel.SSHConfig
}

// NewManager initializes and returns a new Manager
func NewManager(j *postgres.JobService, v *postgres.VirtualMachineService, az *azure.Azure, ssh *sshtunnel.SSHConfig) *Manager {
	s := &Manager{
		jobSvc: j,
		vmSvc:  v,
		az:     az,
		ssh:    ssh,
	}
	return s
}

// Run begins monitoring the database for jobs and processing them
// This function does not return unless an error occurs
func (s *Manager) Run(pollInterval time.Duration) error {
	pollTicker := time.NewTicker(pollInterval)
	maintenanceInterval := pollInterval * 10 // Must be several times longer than polling interval
	maintenanceTicker := time.NewTicker(maintenanceInterval)

	log.Info().Msgf("Service Queue is starting! Polling every %d seconds, maintenance every %d seconds.", int(pollInterval.Seconds()), int(maintenanceInterval.Seconds()))

	// Run forever, handling new job submissions, VM readiness, job deployment, and job completion
	for {
		select {
		case <-pollTicker.C:
			s.ProcessSubmittedJobs()
			s.ProcessAssignedJobs()
			s.ProcessRunningJobs()
			s.DeleteIdleVMs()
		case <-maintenanceTicker.C:
			s.UnassignStalledJobs()
			s.DeleteStalledVMs()
		}
	}
}

// ProcessSubmittedJobs queries the database for newly submitted jobs and begins processing them
func (s *Manager) ProcessSubmittedJobs() error {
	// Fetch unfinished jobs
	jobs, err := s.jobSvc.GetJobsByCurrentStatus(types.Submitted)
	if err != nil {
		log.Error().Err(err).Msg("Manager could not access database")
		return err
	}

	if len(jobs) > 0 {
		// Sum requested resources to choose a VM
		var cores, memory, storage int
		for _, job := range jobs {
			cores += job.Cores
			memory += job.Memory
			storage += job.Storage
		}

		// Choose a new VM name
		vmname := fmt.Sprintf("foundry499-worker-%s", rand.RandomHexString(6))
		log.Info().Msgf("Collected %d submitted jobs requiring %d cores, %d MB memory, %d MB storage; assigning to new VM: %s", len(jobs), cores, memory, storage, vmname)

		// Add the new VM to the database
		vm := types.VirtualMachine{Name: vmname, VMType: types.OnDemand, Cores: cores, Memory: memory, Storage: storage, EstimatedCost: 0.0}
		vmID, err := s.vmSvc.CreateVM(&vm)
		if err != nil {
			log.Error().Err(err).Msg("Failed to add VM to database")
			return err
		}
		vm.ID = vmID

		// Update each job's status and VM assignment
		for _, job := range jobs {
			// Update the job's VirtualMachine assignment in the DB
			job.VirtualMachineID = types.NullInt32{}
			job.VirtualMachineID.Int32 = int32(vmID)
			job.VirtualMachineID.Valid = true
			err := s.jobSvc.UpdateJobByID(job.ID, job, 1) // TODO: set auditID properly
			if err != nil {
				log.Error().Err(err).Msgf("Failed to update job %d", job.ID)
			}
			_, err = s.jobSvc.CreateStatus(&types.JobStatus{JobID: job.ID, StatusCode: types.Assigned})
			if err != nil {
				log.Error().Err(err).Msgf("Failed to update status for job %d", job.ID)
			}
		}

		// Deploy the VM, routine will update the VM in the database when done
		go s.deployVM(&vm)
	}
	return nil
}

// ProcessAssignedJobs queries for assigned jobs with a ready VM and deploys them
func (s *Manager) ProcessAssignedJobs() error {
	// Query for jobs in Assigned state that are assigned to a VM in Ready state
	jobs, err := s.jobSvc.GetAssignedJobsWithReadyVM()
	if err != nil {
		log.Error().Err(err).Msg("Failed to query for assigned jobs with a ready VM")
		return err
	}

	// Group by VM to share an SSH tunnel
	m := make(map[int32][]*types.Job)
	for _, job := range jobs {
		id := job.VirtualMachineID.Int32
		if m[id] == nil {
			m[id] = make([]*types.Job, 0)
		}
		m[id] = append(m[id], job)
	}

	// Deploy jobs by VM
	for _, jobs := range m {
		for i, j := range jobs {
			// Update the job's status in the DB
			_, err := s.jobSvc.CreateStatus(&types.JobStatus{JobID: j.ID, StatusCode: types.Pending})
			if err != nil {
				log.Error().Err(err).Msgf("Failed to update job %d to Pending status", j.ID)
			}
			// Also update the local object before passing it to deployJobs()
			jobs[i].Status = types.Pending
		}
		go s.deployJobs(jobs)
	}

	return nil
}

// ProcessRunningJobs queries for running jobs and checks if they have finished.
// Any jobs which have finished will have their state, logs, and exit code updated in the database.
func (s *Manager) ProcessRunningJobs() error {
	// Query for all jobs in Running status
	jobs, err := s.jobSvc.GetJobsByCurrentStatus(types.Running)
	if err != nil {
		log.Error().Err(err).Msg("Failed to query for running jobs")
		return err
	}

	// Group by VM to share an SSH tunnel
	m := make(map[int32][]*types.Job)
	for _, job := range jobs {
		id := job.VirtualMachineID.Int32
		if m[id] == nil {
			m[id] = make([]*types.Job, 0)
		}
		m[id] = append(m[id], job)
	}

	// Check if jobs are complete (one call per VM)
	for _, v := range m {
		go s.checkJobsComplete(v)
	}

	return nil
}

// DeleteIdleVMs queries for VMs where all assigned jobs have finished and deletes them
func (s *Manager) DeleteIdleVMs() error {
	// Query for VMs in Ready state with no active jobs
	vms, err := s.vmSvc.GetReadyVMsWithNoActiveJobs()
	if err != nil {
		log.Error().Err(err).Msg("Failed to retrieve idle VMs")
		return err
	}

	for _, vm := range vms {
		log.Info().Msgf("VM %d (%s) is idle and will be deleted", vm.ID, vm.Name)
		// deleteVM will transition the VM to Deprovisioning, delete it, then transition it to Removed
		go s.deleteVM(vm)
	}
	return nil
}

// UnassignStalledJobs queries for Jobs stuck in the Assigned or Pending state and reverts them to Submitted to be retried
func (s *Manager) UnassignStalledJobs() error {
	// Query for Jobs in Assigned or Pending state
	assignedJobs, err := s.jobSvc.GetJobsByCurrentStatus(types.Assigned)
	if err != nil {
		log.Error().Err(err).Msg("Failed to retrieve Jobs in Assigned status")
		return err
	}
	pendingJobs, err := s.jobSvc.GetJobsByCurrentStatus(types.Pending)
	if err != nil {
		log.Error().Err(err).Msg("Failed to retrieve Jobs in Pending status")
		return err
	}

	// Check if jobs are stuck, if so revert to Submitted state and unassign them
	for _, job := range assignedJobs {
		s.revertJobIfStuck(job)
	}
	for _, job := range pendingJobs {
		s.revertJobIfStuck(job)
	}
	return nil
}

// DeleteStalledVMs queries for VMs stuck in the Provisioning state and removes them.
// The VMs are required to have no assigned jobs (call UnassignStalledJobs first).
func (s *Manager) DeleteStalledVMs() error {
	// Query for VMs in Provisioning state
	vms, err := s.vmSvc.GetVMsByCurrentStatus(types.Provisioning)
	if err != nil {
		log.Error().Err(err).Msg("Failed to retrieve VMs in Provisioning status")
		return err
	}

	for _, vm := range vms {
		s.deleteVMIfStuck(vm)
	}

	return nil
}
