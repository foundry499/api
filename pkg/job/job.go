package job

import (
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/foundry499/api/pkg/podman"
	"gitlab.com/foundry499/api/pkg/types"
)

// deployJobs connects to a VM and deploys the provided jobs.
// The jobs must all be assigned to the same VM and in the Pending state.
func (s *Manager) deployJobs(jobs []*types.Job) {
	// Sanity checks
	s.sanityCheckJobs(jobs, types.Pending)

	// Look up the VM's IP address
	vm, err := s.vmSvc.GetVMByID(int(jobs[0].VirtualMachineID.Int32))
	if err != nil {
		log.Panic().Err(err).Msgf("Failed to retrieve VM %d from database", vm.ID)
	}

	// Prepare an SSH tunnel to the VM
	tunnel := s.sshTunnel(vm.IPAddress.String)

	// Connect to Podman via the tunnel
	log.Debug().Msgf("Connecting to Podman on VM %d (%s) at %s", vm.ID, vm.Name, vm.IPAddress.String)
	conn := podman.NewConnection(*tunnel)
	defer conn.Close()

	// Deploy each job
	for _, job := range jobs {
		job, err = podman.DeployJob(conn, job)
		if err != nil {
			// Error - revert the job to Assigned so it can be tried again
			_, err = s.jobSvc.CreateStatus(&types.JobStatus{JobID: job.ID, StatusCode: types.Assigned})
			if err != nil {
				log.Panic().Err(err).Msgf("Failed to update job % to assigned status (deployment failed)", job.ID)
			}
			return
		}

		// Update the Job to set its Container ID
		err = s.jobSvc.UpdateJobByID(job.ID, job, 1) // TODO: set audit id properly
		if err != nil {
			log.Panic().Msgf("Failed to update Job %d in database", job.ID)
		}

		// Set status to "Running"
		_, err = s.jobSvc.CreateStatus(&types.JobStatus{JobID: job.ID, StatusCode: types.Running})
		if err != nil {
			log.Panic().Msgf("Failed to update Job %d to running status", job.ID)
		}

		log.Info().Msgf("Job %d (%s) is running on VM %s (container %s)", job.ID, job.Name, vm.Name, job.ContainerID.String)
	}
}

// checkJobsComplete connects to a VM and checks if the provided list of jobs are complete.
// Any jobs which have finished will have their state, logs, and exit code updated in the database.
// All jobs must be running on the same VM.
func (s *Manager) checkJobsComplete(jobs []*types.Job) {
	// Sanity checks
	s.sanityCheckJobs(jobs, types.Running)

	// Look up the VM
	vm, err := s.vmSvc.GetVMByID(int(jobs[0].VirtualMachineID.Int32))
	if err != nil {
		log.Panic().Err(err).Msgf("Failed to retrieve VM %d from database", jobs[0].VirtualMachineID.Int32)
	}
	if !vm.IPAddress.Valid {
		log.Panic().Err(err).Msgf("VM %d does not have a valid IP address", jobs[0].VirtualMachineID.Int32)
	}

	// Prepare an SSH tunnel to the VM
	tunnel := s.sshTunnel(vm.IPAddress.String)

	// Connect to Podman via the tunnel
	log.Debug().Msgf("Connecting to Podman on VM %d (%s) at %s", vm.ID, vm.Name, vm.IPAddress.String)
	conn := podman.NewConnection(*tunnel)
	defer conn.Close()

	// Check each job
	for _, job := range jobs {
		done, job := podman.CheckJobComplete(conn, job)
		if done {
			go s.handleJobComplete(job)
		}
	}
}

// handleJobComplete is called when a deployed job has finished running.
// It updates the job's status and saves the job's logs and exit code to the database.
func (s *Manager) handleJobComplete(job *types.Job) {
	// Update the job to set its logs and exit code
	err := s.jobSvc.UpdateJobByID(job.ID, job, 1) // TODO: set audit ID properly
	if err != nil {
		log.Panic().Err(err).Msgf("Failed to update Job %d in database", job.ID)
	}

	// Update the job's status
	status := types.Failure
	if job.ExitCode.Int32 == 0 {
		status = types.Success
	}
	_, err = s.jobSvc.CreateStatus(&types.JobStatus{JobID: job.ID, StatusCode: status})
	if err != nil {
		log.Panic().Err(err).Msgf("Failed to update Job %d to %s status", job.ID, status.String())
	}

	log.Info().Msgf("Job %d is complete: %s (exit code %d)", job.ID, status.String(), job.ExitCode.Int32)
}

// Performs sanity checks on a slice of jobs and panics unless the following conditions are met:
//  - All jobs are assigned to a VM
//  - All jobs are in the provided state
//  - All jobs are assigned to the same VM
func (s *Manager) sanityCheckJobs(jobs []*types.Job, state types.JobStatusCode) {
	if len(jobs) < 1 {
		log.Panic().Msg("Cannot process empty slice of jobs")
	}
	for _, job := range jobs {
		if !job.VirtualMachineID.Valid {
			log.Panic().Msgf("Expected job %d to have an assigned VM", job.ID)
		}
		if job.Status != state {
			log.Panic().Msgf("Expected job %d to be in %s state (got %s)", job.ID, state.String(), job.Status.String())
		}
		if jobs[0].VirtualMachineID.Int32 != job.VirtualMachineID.Int32 {
			log.Panic().Msgf(
				"Expected job %d to be assigned to VM %d (got VM %d) - all jobs must be on the same VM",
				job.ID,
				jobs[0].VirtualMachineID.Int32,
				job.VirtualMachineID.Int32,
			)
		}
	}
}

func (s *Manager) revertJobIfStuck(job *types.Job) {
	limit := time.Now().Add(-1 * StuckResourceTimeout)

	// Get statuses
	statuses, err := s.jobSvc.GetStatusesByJobID(job.ID)
	if err != nil {
		log.Panic().Err(err).Msgf("Failed to retrieve statuses for Job %d", job.ID)
	}

	// Select latest status
	status := statuses[0]
	for i, st := range statuses {
		if st.CreatedAt.After(status.CreatedAt) {
			status = statuses[i]
		}
	}

	// Check if the latest status is Assigned or Pending
	if status.StatusCode == types.Assigned || status.StatusCode == types.Pending {
		// Check if status is stuck
		if status.CreatedAt.Before(limit) {
			log.Warn().Msgf("Job %d has been %s for more than %d minutes, reverting it to Submitted to be retried", job.ID, status.StatusCode.String(), int(StuckResourceTimeout.Minutes()))

			job.VirtualMachineID = types.NullInt32{}
			job.VirtualMachineID.Valid = false
			err = s.jobSvc.UpdateJobByID(job.ID, job, 1) // TODO: set audit ID properly
			if err != nil {
				log.Panic().Err(err).Msgf("Failed to update job %d (removing VM assignment)", job.ID)
			}
			_, err = s.jobSvc.CreateStatus(&types.JobStatus{JobID: job.ID, StatusCode: types.Submitted})
			if err != nil {
				log.Panic().Err(err).Msgf("Failed to update job %d to Submitted status", job.ID)
			}
		}
	}
}
