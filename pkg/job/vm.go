package job

import (
	"context"
	"fmt"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/foundry499/api/pkg/sshtunnel"
	"gitlab.com/foundry499/api/pkg/types"
)

// deployVM deploys a VM on Azure then updates its status to Ready so it can accept jobs
func (s *Manager) deployVM(vm *types.VirtualMachine) {
	vm, err := s.az.DeployVM(context.Background(), vm)
	if err != nil {
		// Error - set status to "Removed" so assigned jobs can be reassigned
		_, err = s.vmSvc.CreateStatus(&types.VMStatus{VirtualMachineID: vm.ID, StatusCode: types.Removed})
		if err != nil {
			log.Panic().Msgf("Failed to update VM %d to removed status (deployment failure)", vm.ID)
		}
		return
	}

	// Update the VM to set its IP address
	err = s.vmSvc.UpdateVMByID(vm.ID, vm)
	if err != nil {
		log.Panic().Msgf("Failed to update VM %d in database", vm.ID)
	}

	// Set status to "Ready" so it can accept jobs
	_, err = s.vmSvc.CreateStatus(&types.VMStatus{VirtualMachineID: vm.ID, StatusCode: types.Ready})
	if err != nil {
		log.Panic().Msgf("Failed to update VM %d to ready status", vm.ID)
	}

	log.Info().Msgf("Virtual machine %d (%s) is ready with public IP %s", vm.ID, vm.Name, vm.IPAddress.String)
}

// deleteVM deletes a VM from Azure then updates its status to Removed
func (s *Manager) deleteVM(vm *types.VirtualMachine) {
	// Transition the VM status to Deprovisioning so it can be deleted
	_, err := s.vmSvc.CreateStatus(&types.VMStatus{VirtualMachineID: vm.ID, StatusCode: types.Deprovisioning})
	if err != nil {
		log.Panic().Err(err).Msgf("Failed to update VM %d to Deprovisioning status")
	}

	// Delete the VM
	err = s.az.DeleteVM(context.Background(), vm.Name)
	if err != nil {
		log.Panic().Msgf("Failed to delete VM %d", vm.ID)
	}

	// Transition the VM status to Removed
	_, err = s.vmSvc.CreateStatus(&types.VMStatus{VirtualMachineID: vm.ID, StatusCode: types.Removed})
	if err != nil {
		log.Panic().Msgf("Failed to update VM %d to Removed status", vm.ID)
	}
}

func (s *Manager) deleteVMIfStuck(vm *types.VirtualMachine) error {
	limit := time.Now().Add(-1 * StuckResourceTimeout)

	// Check if the VM has been provisioning for more than StuckResourceTimeout
	if vm.CreatedAt.Before(limit) {
		log.Warn().Msgf("VM %d (%s) has been Provisioning for more than %d minutes", vm.ID, vm.Name, int(StuckResourceTimeout.Minutes()))

		// Update the VM's status to Deprovisioning
		_, err := s.vmSvc.CreateStatus(&types.VMStatus{VirtualMachineID: vm.ID, StatusCode: types.Deprovisioning})
		if err != nil {
			log.Error().Err(err).Msgf("Failed to update VM %d to Deprovisioning status", vm.ID)
			return err
		}

		// Ensure there are no jobs assigned to it
		jobs, err := s.jobSvc.GetJobsByVirtualMachine(vm.ID)
		if err != nil {
			log.Error().Err(err).Msgf("Failed to retrieve jobs assigned to VM %d", vm.ID)
			return err
		}
		if len(jobs) > 0 {
			err = fmt.Errorf("refusing to delete stuck VM %d because it has assigned jobs - unassign the jobs then try again", vm.ID)
			log.Error().Err(err)
			return err
		}

		// Delete the VM
		go s.deleteVM(vm)
	}
	return nil
}

// sshTunnel instantiates a new SSHTunnel with the provided IP.
// It is a convenience wrapper on sshtunnel.NewSSHTunnel().
func (s *Manager) sshTunnel(ipAddress string) *sshtunnel.SSHTunnel {
	return sshtunnel.NewSSHTunnel(
		fmt.Sprintf("tcp://%s@%s", s.ssh.User, ipAddress),
		sshtunnel.PublicKeyFromString(s.ssh.PrivateKey),
		s.ssh.Socket,
		"0", // 0: choose a random available local port
	)
}
