package http

import (
	"net/http"

	"github.com/rs/zerolog/log"
)

// LoggerMiddleware implements mux.MiddlewareFunc.
func LoggerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Info().
			Str("Method", r.Method).
			Str("Path", r.URL.Path).
			Msg("Received Request")
		next.ServeHTTP(w, r)
	})
}
