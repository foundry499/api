package http

import (
	"encoding/json"
	"net/http"
)

// GetAllVMHandler godoc
// @Summary Get All VM's
// @Description Gets all vm objects. VM's are exlusively created by the backend and there is no API to create them
// @Tags vm
// @Accept json
// @Produce json
// @Success 200 {object} types.VirtualMachine
// @Router /vms [get]
func GetAllVMHandler(w http.ResponseWriter, r *http.Request) {
	res, err := s.VMService.GetVMs()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(res)
}

// GetAllVMStatusHandler godoc
// @Summary Get All VM History
// @Description Gets all vm status objects. VMStatuses are exlusively created by the backend and there is no API to create them
// @Tags vm
// @Accept json
// @Produce json
// @Success 200 {object} types.VMStatus
// @Router /vms/status [get]
func GetAllVMStatusHandler(w http.ResponseWriter, r *http.Request) {
	res, err := s.VMService.GetVMStatuses()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(res)
}
