package http

import (
	"encoding/json"
	"strconv"

	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"gitlab.com/foundry499/api/pkg/auth"
	"gitlab.com/foundry499/api/pkg/types"
)

// GetUserHandler godoc
// @Summary Get user
// @Description Gets user object if it exists
// @Tags user
// @Accept json
// @Produce json
// @Param id path int true "The user id" example(1)
// @Success 200 {object} types.User
// @Router /user/{id} [get]
func GetUserHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	u, err := s.UserService.GetUserByID(id)
	json.NewEncoder(w).Encode(u)
}

// CreateUserHandler godoc
// @Summary Create user
// @Description Creates a new user
// @Tags user
// @Accept json
// @Produce json
// @Param NewUser body types.NewUser true "new user info"
// @Success 200 {object} types.User
// @Router /user/ [post]
func CreateUserHandler(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value(contextKeyUserRole) != types.AdminRole {
		http.Error(w, "Only admins can create new users", http.StatusUnauthorized)
		return
	}
	contextID := r.Context().Value(contextKeyUserID).(int)

	var m map[string]string
	var u types.User // TODO: use NewUser model instead

	err := json.NewDecoder(r.Body).Decode(&m)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if m["email"] != "" {
		u.Email = m["email"]
	}
	if m["name"] != "" {
		u.Name = m["name"]
	}
	switch m["role"] {
	case "admin":
		u.Role = types.AdminRole
	case "base":
		u.Role = types.BaseRole
	default:
		u.Role = types.GuestRole // default user role is guest
	}
	if m["password"] != "" {
		u.PasswordHash, err = auth.HashPassword(m["password"])
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	id, err := s.UserService.CreateUser(&u, contextID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	newUser, err := s.UserService.GetUserByID(id)
	json.NewEncoder(w).Encode(newUser)
}

// UpdateUserHandler godoc
// @Summary Update user
// @Description Updates user info. Only updates fields that are included.
// @Tags user
// @Accept json
// @Produce json
// @Param id path int true "The user id" example(1)
// @Param UpdateUser body types.NewUser true "user info to be updated"
// @Success 200 {object} types.User
// @Router /user/{id} [put]
func UpdateUserHandler(w http.ResponseWriter, r *http.Request) {
	contextRole := r.Context().Value(contextKeyUserRole)
	contextID := r.Context().Value(contextKeyUserID).(int)

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	// check if this user should be allowed to perform this op
	if contextID != id && contextRole != types.AdminRole {
		log.Info().Msgf("id %d", contextID)

		http.Error(w, "Cannot update other user", http.StatusUnauthorized)
		return
	}

	// map contains key:value of json body
	var m map[string]string
	err = json.NewDecoder(r.Body).Decode(&m)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Get existing user so we only overwrite new data
	u, err := s.UserService.GetUserByID(id)

	if m["email"] != "" {
		u.Email = m["email"]
	}
	if m["name"] != "" {
		u.Name = m["name"]
	}
	if m["role"] != "" {
		switch m["role"] {
		case "admin":
			u.Role = types.AdminRole
		case "base":
			u.Role = types.BaseRole
		default:
			u.Role = types.GuestRole // default user role is guest
		}
	}
	if m["password"] != "" {
		u.PasswordHash, err = auth.HashPassword(m["password"])
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}

	err = s.UserService.UpdateUserByID(id, u, contextID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

// DeleteUserHandler godoc
// @Summary Delete user info
// @Description Deletes user info. Functionality is safe - users can be deleted without causing reference errors
// @Tags user
// @Accept json
// @Produce json
// @Param id path int true "The user id" example(1)
// @Success 200
// @Router /user/{id} [delete]
func DeleteUserHandler(w http.ResponseWriter, r *http.Request) {
	// only admins can delete users as it will probably break something
	if r.Context().Value(contextKeyUserRole) != types.AdminRole {
		http.Error(w, "Only admins can delete users", http.StatusUnauthorized)
		return
	}
	contextID := r.Context().Value(contextKeyUserID).(int)

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = s.UserService.DeleteUser(id, contextID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

// GetUsersHandler godoc
// @Summary Get all users
// @Description Returns list of all active users. Deleted users are not included
// @Tags user
// @Accept json
// @Produce json
// @Success 200 {array} types.User
// @Router /users [get]
func GetUsersHandler(w http.ResponseWriter, r *http.Request) {
	u, err := s.UserService.GetAllUsers()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(u)
}
