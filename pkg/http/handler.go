package http

import (
	"net/http"
	"strconv"

	// docs location for swagger
	_ "gitlab.com/foundry499/api/docs"

	"github.com/rs/cors"
	"github.com/rs/zerolog/log"
	httpSwagger "github.com/swaggo/http-swagger"
	"gitlab.com/foundry499/api/pkg/types"

	"github.com/gorilla/mux"
)

var s *Services

// @title Foundry499 API
// @version 0.0.1
// @description This API provides the external Foundry 499 interface.
// @website https://www.foundry499.com

// @contact.name Tyler Harnadek
// @contact.email tharnadek@hey.com
// @contact.url https://www.foundry499.com

// @host api.foundry499.com
// @BasePath /v1

// Services provides access to DB services for accessing domain types
type Services struct {
	UserService types.UserService
	JobService  types.JobService
	VMService   types.VirtualMachineService
}

func getRouter() *mux.Router {
	r := mux.NewRouter()
	// apply logger to all routes
	r.Use(LoggerMiddleware)
	// setup docs router
	r.PathPrefix("/docs/").Handler(httpSwagger.WrapHandler)

	// session router
	r.HandleFunc("/login", LoginHandler).Methods("POST")
	r.HandleFunc("/logout", LogoutHandler).Methods("POST")
	r.HandleFunc("/refresh", RefreshSessionHandler).Methods("POST")

	// api router
	apiRouter := r.PathPrefix("/v1").Subrouter()
	// api routes are authenticated
	apiRouter.Use(Authenticated)
	// user routes
	apiRouter.HandleFunc("/user/{id}", GetUserHandler).Methods("GET")
	apiRouter.HandleFunc("/user/{id}", UpdateUserHandler).Methods("PUT")
	apiRouter.HandleFunc("/user/{id}", DeleteUserHandler).Methods("DELETE")
	apiRouter.HandleFunc("/user", CreateUserHandler).Methods("POST")
	apiRouter.HandleFunc("/users", GetUsersHandler).Methods("GET")
	// job routes
	apiRouter.HandleFunc("/job/{id}", GetJobHandler).Methods("GET")
	apiRouter.HandleFunc("/job/{id}", UpdateJobHandler).Methods("PUT")
	apiRouter.HandleFunc("/job/{id}", DeleteJobHandler).Methods("DELETE")
	apiRouter.HandleFunc("/job/{id}/status", GetJobStatusHandler).Methods("GET")
	apiRouter.HandleFunc("/job/{id}/status", CreateJobStatusHandler).Methods("POST")
	apiRouter.HandleFunc("/job", CreateJobHandler).Methods("POST")
	apiRouter.HandleFunc("/jobs", GetJobsHandler).Methods("GET")
	apiRouter.HandleFunc("/jobs/status", GetAllStatusHandler).Methods("GET")

	apiRouter.HandleFunc("/vms", GetJobsHandler).Methods("GET")
	apiRouter.HandleFunc("/vms/status", GetAllStatusHandler).Methods("GET")
	return r
}

// Start initializes handler
func Start(p int, services *Services) {
	// port argument needs to be in format ":8080"
	port := ":" + strconv.Itoa(p)
	// setup router
	r := getRouter()
	// put services into global scope
	s = services
	// start the server
	log.Print("listening on port ==> ", p)

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:3000", "http://app.foundry499.com"},
		AllowedMethods:   []string{http.MethodGet, http.MethodPost, http.MethodDelete, http.MethodPut, http.MethodOptions},
		AllowCredentials: true,
	})

	err := http.ListenAndServe(port, c.Handler(r))
	if err != nil {
		log.Fatal().Err(err)
	}
}
