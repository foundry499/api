package http

import (
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/gorilla/sessions"
	"github.com/rs/zerolog/log"
	"gitlab.com/foundry499/api/pkg/auth"
)

// defining a contextKey in this manner prevents name collision in global context
type contextKey string

func (c contextKey) String() string {
	return "foundry499/api/pkg/auth_context_" + string(c)
}

var (
	store              = sessions.NewCookieStore(getKey())
	contextKeyUserRole = contextKey("userRole")
	contextKeyUserID   = contextKey("userID")
)

func getKey() []byte {
	key, bool := os.LookupEnv("SESSION_KEY")
	if !bool {
		// don't handle a lack of session key - just fail
		log.Warn().Msg("SESSION_KEY Environment Variable not set - using insecure development key.")
		key = "INSECURE_DEVELOPMENT_KEY"
	}
	return []byte(key)
}

// Credentials represents the login request body
type Credentials struct {
	Password string `json:"password"`
	Email    string `json:"email"`
}

// Authenticated implements mux.MiddlewareFunc.
func Authenticated(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := store.Get(r, "foundry-cookie")

		if err != nil {
			// user has no session
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
			// session exists but is not authenticated
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		id, ok := session.Values["id"].(int)
		if !ok {
			// possible id is stored as something other than int
			http.Error(w, "Internal Error", http.StatusInternalServerError)
			return
		}

		u, err := s.UserService.GetUserByID(id)

		if err != nil {
			// possible db issue
			http.Error(w, "User could not be accessed", http.StatusInternalServerError)
			return
		}

		// pass user info to context for route handlers
		ctx := r.Context()
		ctx = context.WithValue(ctx, contextKeyUserID, id)
		ctx = context.WithValue(ctx, contextKeyUserRole, u.Role)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// RefreshSessionHandler godoc
// @Summary Refresh session
// @Description Checks cookie validity and freshens cookie if it is valid
// @Tags auth
// @Produce json
// @Success 200 {object} types.User
// @Router /refresh [post]
func RefreshSessionHandler(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, "foundry-cookie")
	session.Options = &sessions.Options{
		Path:   "/",
		MaxAge: 600,
		// Secure:   true, // implemented by caddy reverse proxy
		HttpOnly: true,
		SameSite: http.SameSiteNoneMode,
	}

	if err != nil {
		// user has no session
		http.Error(w, "Session does not exist", http.StatusUnauthorized)
		return
	}

	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		// session exists but is not authenticated
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	id := session.Values["id"].(int)
	u, err := s.UserService.GetUserByID(id)

	// ensure the user exists in db and has email and password
	if err != nil || u.Email == "" || u.PasswordHash == "" {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}
	session.Save(r, w)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(u)
}

// LoginHandler godoc
// @Summary Log in
// @Description Checks credentials and issues a cookie if valid
// @Tags auth
// @Produce json
// @Param cred body Credentials true "credential info"
// @Success 200 {object} types.User
// @Router /login [post]
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "foundry-cookie")
	session.Options = &sessions.Options{
		Path:   "/",
		MaxAge: 600,
		// Secure:   true, // implemented by caddy reverse proxy
		HttpOnly: true,
		SameSite: http.SameSiteNoneMode,
	}

	// TODO ADD ERROR HANDLING

	cred := &Credentials{}

	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576)) // use limitreader to prevent giant input attack
	if err != nil {
		http.Error(w, "Error reading request", http.StatusInternalServerError)
		return
	}
	if err := r.Body.Close(); err != nil {
		http.Error(w, "Error reading request", http.StatusInternalServerError)
		return
	}
	if err := json.Unmarshal(body, &cred); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			http.Error(w, "Error reading request", http.StatusInternalServerError)
		}
		return
	}

	// password and email required
	if cred.Password == "" || cred.Email == "" {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	// ensure the user exists in db and has email and password
	u, err := s.UserService.GetUserByEmail(cred.Email)
	if err != nil || u.Email == "" || u.PasswordHash == "" {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	if !auth.CheckPasswordHash(cred.Password, u.PasswordHash) {
		http.Error(w, "Bad Request", http.StatusUnauthorized)
		return
	}

	// Set user as authenticated
	session.Values["authenticated"] = true
	session.Values["email"] = u.Email
	session.Values["id"] = u.ID
	session.Save(r, w)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(u)
}

// LogoutHandler godoc
// @Summary Log out
// @Description Invalidates cookie if it exists
// @Tags auth
// @Success 200
// @Router /logout [post]
func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "foundry-cookie")
	session.Options = &sessions.Options{
		Path:   "/",
		MaxAge: 600,
		// Secure:   true, // implemented by caddy reverse proxy
		HttpOnly: true,
		SameSite: http.SameSiteNoneMode,
	}

	// Revoke users authentication
	session.Values["authenticated"] = false
	session.Save(r, w)
}
