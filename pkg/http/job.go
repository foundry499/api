package http

import (
	"encoding/json"
	"io/ioutil"
	"strconv"

	"net/http"

	"gitlab.com/foundry499/api/pkg/types"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
)

// NewJob is submitted via the new job route
// Name and Tags are mutable, all other fields are consistent once added to DB
type NewJob struct {
	Name         string          `json:"name" db:"job_name"`
	Tags         json.RawMessage `json:"tags" db:"tags"`
	DockerImage  string          `json:"dockerImage" db:"docker_image"`
	StartCommand string          `json:"startCommand" db:"start_command"`
	EnvVars      json.RawMessage `json:"envVars" db:"env_vars"`
	VMType       types.VMType    `json:"VMType" db:"vm_type"`
	Storage      int             `json:"storage" db:"storage"`
	Cores        int             `json:"cores" db:"cores"`
	Memory       int             `json:"memory" db:"memory"`
}

// UpdateJob is submitted via the update job route
// Name and Tags are mutable, all other fields are consistent once added to DB
type UpdateJob struct {
	Name string          `json:"name" db:"job_name"`
	Tags json.RawMessage `json:"tags" db:"tags"`
}

// NewJobStatus is submitted via the update job route
type NewJobStatus struct {
	JobID      int                 `json:"jobID"`
	StatusCode types.JobStatusCode `json:"status"`
}

// GetJobHandler godoc
// @Summary Get job
// @Description Gets job object if it exists
// @Tags job
// @Accept json
// @Produce json
// @Param id path int true "The job id" example(1)
// @Success 200 {object} types.Job
// @Router /job/{id} [get]
func GetJobHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	j, err := s.JobService.GetJobByID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	json.NewEncoder(w).Encode(j)
}

// CreateJobHandler godoc
// @Summary Create job
// @Description Creates a new job
// @Tags job
// @Accept json
// @Produce json
// @Param NewJob body NewJob true "new job info"
// @Success 200 {object} types.Job
// @Router /job/ [post]
func CreateJobHandler(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value(contextKeyUserRole) == types.GuestRole {
		http.Error(w, "Guest users can't create jobs", http.StatusUnauthorized)
		return
	}
	contextID := r.Context().Value(contextKeyUserID).(int)

	var j types.Job

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	json.Unmarshal(body, &j)

	id, err := s.JobService.CreateJob(&j, contextID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// status 0 = Submitted
	status := types.JobStatus{
		StatusCode: 0,
		JobID:      id,
	}
	_, err = s.JobService.CreateStatus(&status)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	newUser, err := s.JobService.GetJobByID(id)
	json.NewEncoder(w).Encode(newUser)
}

// UpdateJobHandler godoc
// @Summary Update job
// @Description Updates job info. Only updates fields that are included in the request
// @Tags job
// @Accept json
// @Produce json
// @Param id path int true "The job id" example(1)
// @Param UpdateJob body UpdateJob true "job info to be updated"
// @Success 200 {object} types.Job
// @Router /job/{id} [put]
func UpdateJobHandler(w http.ResponseWriter, r *http.Request) {
	contextRole := r.Context().Value(contextKeyUserRole)
	contextID := r.Context().Value(contextKeyUserID).(int)

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	// get the job
	j, err := s.JobService.GetJobByID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	// check if this user should be allowed to perform this op
	if contextID != j.CreatedBy && contextRole != types.AdminRole {
		log.Info().Msgf("id %d", contextID)

		http.Error(w, "Cannot update other user's job unless you are admin", http.StatusUnauthorized)
		return
	}

	var next types.Job

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	json.Unmarshal(body, &next)

	// only include name and tags if they are included in request
	if next.Name != "" {
		j.Name = next.Name
	}
	if next.Tags != nil {
		j.Tags = next.Tags
	}
	// fields other than name and tags are not mutable in this db

	err = s.JobService.UpdateJobByID(id, j, contextID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

// DeleteJobHandler godoc
// @Summary Delete job info
// @Description Deletes job info.
// @Tags job
// @Accept json
// @Produce json
// @Param id path int true "The job id" example(1)
// @Success 200
// @Router /job/{id} [delete]
func DeleteJobHandler(w http.ResponseWriter, r *http.Request) {
	// only admins can delete jobs as it will probably break something
	if r.Context().Value(contextKeyUserRole) != types.AdminRole {
		http.Error(w, "Only admins can delete jobs", http.StatusUnauthorized)
		return
	}
	contextID := r.Context().Value(contextKeyUserID).(int)

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = s.JobService.DeleteJob(id, contextID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

// GetJobsHandler godoc
// @Summary Get all jobs
// @Description Returns list of all active jobs. Deleted jobs are not included
// @Tags job
// @Accept json
// @Produce json
// @Success 200 {array} types.Job
// @Router /jobs [get]
func GetJobsHandler(w http.ResponseWriter, r *http.Request) {
	j, err := s.JobService.GetAllJobs()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(j)
}

// GetJobStatusHandler godoc
// @Summary Get Job History
// @Description Gets all job status objects associated with the given job
// @Tags job
// @Accept json
// @Produce json
// @Param id path int true "The job id" example(1)
// @Success 200 {object} types.JobStatus
// @Router /job/{id}/status [get]
func GetJobStatusHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	js, err := s.JobService.GetStatusesByJobID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(js)
}

// GetAllStatusHandler godoc
// @Summary Get All Job History
// @Description Gets all job status objects
// @Tags job
// @Accept json
// @Produce json
// @Param id path int true "The job id" example(1)
// @Success 200 {object} types.JobStatus
// @Router /jobs/status [get]
func GetAllStatusHandler(w http.ResponseWriter, r *http.Request) {
	js, err := s.JobService.GetAllJobStatuses()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(js)
}

// CreateJobStatusHandler godoc
// @Summary Create job job status
// @Description Creates a new job status object
// @Tags job
// @Accept json
// @Produce json
// @Param NewJobStatus body NewJobStatus true "new job status object details"
// @Success 200
// @Router /job/{id}/status [post]
func CreateJobStatusHandler(w http.ResponseWriter, r *http.Request) {
	js := types.JobStatus{}
	// map contains key:value of json body
	var m map[string]string
	err := json.NewDecoder(r.Body).Decode(&m)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if m["job_id"] != "" {
		i, err := strconv.Atoi(m["job_id"])
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		js.JobID = i
	}
	if m["status_code"] != "" {
		i, err := strconv.Atoi(m["status_code"])
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		js.StatusCode = types.JobStatusCode(i)
	}

	_, err = s.JobService.CreateStatus(&js)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
