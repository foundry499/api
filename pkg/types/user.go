package types

// User is a domain type
type User struct {
	Audit
	ID           int      `json:"id" db:"id"`
	Name         string   `json:"name" db:"full_name"`
	Email        string   `json:"email" db:"email"`
	Role         UserRole `json:"role" db:"role"`
	PasswordHash string   `json:"-" db:"password_hash"`
}

// NewUser struct is used for creating a user
type NewUser struct {
	Name     string   `json:"name"`
	Email    string   `json:"email"`
	Role     UserRole `json:"role"`
	Password string   `json:"password"`
}

// UserService implements the User domain type
type UserService interface {
	GetUserByID(id int) (*User, error)
	GetUserByEmail(email string) (*User, error)
	GetAllUsers() ([]*User, error)
	CreateUser(u *User, auditID int) (int, error) // return int is the id of created user
	DeleteUser(id int, auditID int) error
	UpdateUserByID(id int, u *User, auditID int) error
}
