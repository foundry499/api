package types

import (
	"time"
)

// VMType indicates which type of Azure VM is used
type VMType int

const (
	// OnDemand Azure VM type
	OnDemand VMType = iota
	// Spot Azure VM type
	Spot
	// LowPriority Azure VM type
	LowPriority
)

// VMStatusCode enum represents VM status
type VMStatusCode int

const (
	// Provisioning indicates a VM is in the process of being created
	Provisioning VMStatusCode = iota
	// Ready indicates a VM is running and ready to accept jobs
	Ready
	// Deprovisioning indicates a VM has been marked for destruction and cannot accept new jobs
	Deprovisioning
	// Removed indicates a VM has been destroyed
	Removed
)

// VMStatus is a domain type
type VMStatus struct {
	ID               int          `json:"id" db:"id"`
	VirtualMachineID int          `json:"vmID" db:"virtualmachine_id"`
	StatusCode       VMStatusCode `json:"status" db:"status_code"`
	CreatedAt        time.Time    `json:"createdAt" db:"created_at"`
}

// VirtualMachine is a domain type
// EstimatedCost, IPAddress, and Status are mutable, all other fields are immutable once created
type VirtualMachine struct {
	ID            int          `json:"id" db:"id"`
	Name          string       `json:"name" db:"vm_name"`
	Cores         int          `json:"cores" db:"cores"`
	Memory        int          `json:"memory" db:"memory"`
	Storage       int          `json:"storage" db:"storage"`
	EstimatedCost float32      `json:"estimatedCost" db:"estimated_cost"`
	VMType        VMType       `json:"vmType" db:"vm_type"`
	Status        VMStatusCode `json:"status" db:"status_code"`
	IPAddress     NullString   `json:"ip" db:"ip_address"` // populated when Status is Ready
	CreatedAt     time.Time    `json:"createdAt" db:"created_at"`
	UpdatedAt     NullTime     `json:"updatedAt" db:"updated_at"`
}

// VirtualMachineService implements the VirtualMachine domain type
type VirtualMachineService interface {
	GetVMByID(id int) (*VirtualMachine, error)
	CreateVM(v *VirtualMachine) (int, error)    // returned int is new VM id
	CreateStatus(status *VMStatus) (int, error) // returned id is new status id
	GetVMs() ([]*VirtualMachine, error)
	GetReadyVMsWithNoActiveJobs() ([]*VirtualMachine, error)
	GetVMsByCurrentStatus(status VMStatusCode) ([]*VirtualMachine, error)
	GetVMStatuses() ([]*VMStatus, error)
}
