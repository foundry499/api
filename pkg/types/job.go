package types

import (
	"encoding/json"
	"time"
)

// JobStatusCode enum represents processing task status
type JobStatusCode int

const (
	// Submitted the processing job has been submitted by has not been assessed by the service queue
	Submitted JobStatusCode = iota
	// Assigned the job has been assessed by the service queue and assigned to a VM (the VM is not ready yet)
	Assigned
	// Pending the assigned VM is ready and the service queue is deploying the job to it
	Pending
	// Running the job was successfully deployed to a VM and has started
	Running
	// Success the job has finished executing successfully (no errors)
	Success
	// Failure the job has finished executing with a failure status (errors occurred)
	Failure
)

func (j JobStatusCode) String() string {
	return [...]string{"Submitted", "Assigned", "Pending", "Running", "Success", "Failure"}[j]
}

// Job is a domain type
type Job struct {
	Audit
	ID               int             `json:"id" db:"id"`
	Name             string          `json:"name" db:"job_name"`
	Tags             json.RawMessage `json:"tags" db:"tags"`
	DockerImage      string          `json:"dockerImage" db:"docker_image"`
	StartCommand     string          `json:"startCommand" db:"start_command"`
	EnvVars          json.RawMessage `json:"envVars" db:"env_vars"`
	VMType           VMType          `json:"VMType" db:"vm_type"`
	Storage          int             `json:"storage" db:"storage"`
	Cores            int             `json:"cores" db:"cores"`
	Memory           int             `json:"memory" db:"memory"`
	Status           JobStatusCode   `json:"status" db:"status_code"`
	VirtualMachineID NullInt32       `json:"virtualMachine" db:"virtualmachine_id"` // populated once job is accepted (Status is Assigned)
	ContainerID      NullString      `json:"containerId" db:"container_id"`         // populated once job is started (Status is Running)
	LogsStdout       []byte          `json:"logsStdout" db:"logs_stdout"`           // populated once job is complete (Status is Success or Failure)
	LogsStderr       []byte          `json:"logsStderr" db:"logs_stderr"`           // populated once job is complete (Status is Success or Failure)
	ExitCode         NullInt32       `json:"exitCode" db:"exit_code"`               // populated once job is complete (Status is Success or Failure)
}

// JobStatus is a domain type
type JobStatus struct {
	ID         int           `json:"id" db:"id"`
	JobID      int           `json:"jobID" db:"job_id"`
	StatusCode JobStatusCode `json:"status" db:"status_code"`
	CreatedAt  time.Time     `json:"createdAt" db:"created_at"`
}

// JobService implements the User domain type
type JobService interface {
	GetJobByID(id int) (*Job, error)
	GetJobsByUser(id int) ([]*Job, error)
	GetAllJobs() ([]*Job, error)
	GetAssignedJobsWithReadyVM() ([]*Job, error)
	GetJobsByCurrentStatus(status JobStatusCode) ([]*Job, error)
	GetJobsByVirtualMachine(id int) ([]*Job, error)

	CreateJob(j *Job, auditID int) (int, error) // returned int is new job id
	DeleteJob(id int, auditID int) error
	UpdateJobByID(id int, u *Job, auditID int) error

	CreateStatus(status *JobStatus) (int, error) // returned id is new status id
	GetStatusesByJobID(id int) ([]*JobStatus, error)
	GetAllJobStatuses() ([]*JobStatus, error)
}
