package types

import (
	"database/sql"
	"encoding/json"
)

// NullInt64 is the same as sql.NullInt64
// But when exported to JSON it is null or the int64 val that is exported
// not a JSON containing Value and Valid properties
type NullInt64 struct {
	sql.NullInt64
}

// MarshalJSON NullInt64 interface redefinition
func (r NullInt64) MarshalJSON() ([]byte, error) {
	if r.Valid {
		return json.Marshal(r.Int64)
	}
	return json.Marshal(nil)
}

// NullInt32 is the same as sql.NullInt32
// But when exported to JSON it is null or the int64 val that is exported
// not a JSON containing Value and Valid properties
type NullInt32 struct {
	sql.NullInt32
}

// MarshalJSON NullInt64 interface redefinition
func (r NullInt32) MarshalJSON() ([]byte, error) {
	if r.Valid {
		return json.Marshal(r.Int32)
	}
	return json.Marshal(nil)
}

// NullTime is the same as sql.NullTime
// But when exported to JSON it is null or the time val that is exported
// not a JSON containing Time and Valid properties
type NullTime struct {
	sql.NullTime
}

// MarshalJSON NullTime interface redefinition
func (r NullTime) MarshalJSON() ([]byte, error) {
	if r.Valid {
		return json.Marshal(r.Time)
	}
	return json.Marshal(nil)
}

// NullString is the same as sql.String
// But when exported to JSON it is null or the time val that is exported
// not a JSON containing Time and Valid properties
type NullString struct {
	sql.NullString
}

// MarshalJSON NullInt64 interface redefinition
func (r NullString) MarshalJSON() ([]byte, error) {
	if r.Valid {
		return json.Marshal(r.String)
	}
	return json.Marshal(nil)
}
