package types

import (
	"time"
)

// Audit is a base type used in db rows
type Audit struct {
	CreatedBy int       `json:"createdBy" db:"created_by"`
	CreatedAt time.Time `json:"createdAt" db:"created_at"`
	UpdatedBy NullInt64 `json:"updatedBy" db:"updated_by"`
	UpdatedAt NullTime  `json:"updatedAt" db:"updated_at"`
	IsDeleted bool      `json:"isDeleted" db:"is_deleted" json:"-"`
}
