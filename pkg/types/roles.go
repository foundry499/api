package types

// UserRole defines types of users
type UserRole string

const (
	// AdminRole assigned to system administrators, has * privileges
	AdminRole UserRole = "admin"
	// BaseRole assigned to system administrators, has base privileges
	BaseRole UserRole = "base"
	// GuestRole assigned to guests, has read-only privileges
	GuestRole UserRole = "guest"
)
