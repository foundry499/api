package podman

import (
	"encoding/json"

	"github.com/rs/zerolog/log"
	"gitlab.com/foundry499/api/pkg/types"
)

// DeployJob connects to a VM via the provided connection and starts a container on it.
func DeployJob(conn *Connection, job *types.Job) (*types.Job, error) {
	// Ping to ensure we are connected
	err := conn.Ping()
	if err != nil {
		log.Panic().Err(err).Msgf("Failed to ping podman at %s", conn.tunnel.Server.Host)
	}

	// Pull the image needed by this job
	err = conn.PullImage(job.DockerImage)
	if err != nil {
		log.Error().Err(err).Msg("Failed to pull Docker image")
		return job, err
	}
	log.Debug().Msgf("Podman: pulled image %s", job.DockerImage)

	// Create the container
	id, err := conn.CreateContainer(job.DockerImage, job.EnvVars)
	if err != nil {
		log.Error().Err(err).Msg("Failed to create container")
		return job, err
	}
	log.Debug().Msgf("Podman: created container %s", id)

	// Start the container
	err = conn.StartContainer(id)
	if err != nil {
		log.Error().Err(err).Msg("Failed to start container")
		return job, err
	}
	log.Debug().Msgf("Podman: started container %s", id)

	// The job is running, update its container ID
	job.ContainerID = types.NullString{}
	job.ContainerID.String = id
	job.ContainerID.Valid = true
	return job, nil
}

// CheckJobComplete checks whether a job is finished running.
// Returns bool indicating whether the job is done, and the job object.
// If finished, the returned job's ExitCode, LogsStdout, and LogsStderr properties are set.
func CheckJobComplete(conn *Connection, job *types.Job) (bool, *types.Job) {
	// Ping to ensure we are connected
	err := conn.Ping()
	if err != nil {
		log.Panic().Err(err).Msgf("Failed to ping podman at %s", conn.tunnel.Server.Host)
	}

	// Check if the job is finished
	res, err := conn.ContainerInfo(job.ContainerID.String)
	if err != nil {
		log.Panic().Err(err).Msgf("Failed to retrieve container info for job %d", job.ID)
	}
	var result map[string]interface{}
	json.Unmarshal([]byte(res), &result)

	state := result["State"].(map[string]interface{})
	status := state["Status"].(string)
	exitCode := int32(state["ExitCode"].(float64)) // TODO: any idea why Go sees this int as a float64?

	if status == "exited" {
		// Job is done! Populate results
		job.ExitCode = types.NullInt32{}
		job.ExitCode.Int32 = exitCode
		job.ExitCode.Valid = true
		job.LogsStdout, err = conn.ContainerLogsStdout(job.ContainerID.String)
		if err != nil {
			log.Error().Err(err).Msgf("Failed to retrieve container logs (stdout) for job %d", job.ID)
		}
		job.LogsStderr, err = conn.ContainerLogsStderr(job.ContainerID.String)
		if err != nil {
			log.Error().Err(err).Msgf("Failed to retrieve container logs (stderr) for job %d", job.ID)
		}
		return true, job
	}
	return false, job
}
