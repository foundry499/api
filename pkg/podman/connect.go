package podman

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/foundry499/api/pkg/sshtunnel"
)

// Connection holds connection info to a remote Podman worker
type Connection struct {
	tunnel    sshtunnel.SSHTunnel
	netclient http.Client
	baseurl   string
}

// Close shuts down the SSH tunnel to the Podman worker
func (c *Connection) Close() {
	c.tunnel.Close()
}

// Ping ensures the connection to Podman is alive
func (c *Connection) Ping() error {
	_, err := c.netclient.Head(c.baseurl + "/libpod/_ping")
	if err != nil {
		return err
	}
	return nil
}

// SystemInfo gathers information on the Podman worker
func (c *Connection) SystemInfo() ([]byte, error) {
	resp, err := c.netclient.Get(c.baseurl + "/v1.0.0/libpod/info")
	if err != nil {
		return make([]byte, 0), err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return make([]byte, 0), err
	}
	return body, nil
}

// PullImage instructs Podman to pull a remote image
func (c *Connection) PullImage(reference string) error {
	// Construct URL
	url := fmt.Sprintf("%s/v1.0.0/libpod/images/pull?reference=%s", c.baseurl, url.QueryEscape(reference))

	// Make request
	resp, err := c.netclient.Post(url, "", nil)
	if err != nil {
		log.Error().Err(err).Msg("Failed to send POST request to pull image")
		return err
	}
	// Body must be read to completion and closed to keep client healthy for the next request
	ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	if resp.StatusCode != 200 {
		err = fmt.Errorf("Podman replied to pull image request with status code %d (expected 200)", resp.StatusCode)
		log.Error().Err(err)
		return err
	}

	// No errors, image pulled
	return nil
}

// CreateContainer creates a new container but does not start it
func (c *Connection) CreateContainer(image string, environment json.RawMessage) (string, error) {
	// Sanity-check the environment variables
	if !json.Valid(environment) {
		log.Error().Msg("Environment variables do not parse as valid JSON")
		return "", errors.New("Environment variables do not parse as valid JSON")
	}

	// Prepare the request
	body := []byte(fmt.Sprintf(`{"image": "%s"}`, image))
	url := c.baseurl + "/v1.0.0/libpod/containers/create"

	// Send the request
	resp, err := c.netclient.Post(url, "application/json", bytes.NewBuffer(body))
	if err != nil {
		log.Error().Err(err).Msg("Failed to send POST request to create container")
		return "", err
	}
	defer resp.Body.Close()

	// Read the response
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	type Response struct {
		ID       string `json:"Id"`
		Warnings []string
	}
	var res Response
	err = json.Unmarshal(responseBody, &res)
	if err != nil {
		log.Error().Err(err).Msg("Failed to unmarshal response body")
		return "", err
	}

	// Check status
	if resp.StatusCode != 201 {
		err = fmt.Errorf("Podman replied to create container request with status code %d (expected 201)", resp.StatusCode)
		log.Error().Err(err)
		return "", err
	}

	// Check for warnings from Podman
	if len(res.Warnings) > 0 {
		log.Warn().Strs("warnings", res.Warnings).Msg("Podman returned warnings on container creation")
	}

	return res.ID, nil
}

// StartContainer instructs Podman to start an existing container
func (c *Connection) StartContainer(id string) error {
	url := fmt.Sprintf("%s/v1.0.0/libpod/containers/%s/start", c.baseurl, id)
	resp, err := c.netclient.Post(url, "", nil)
	if err != nil {
		return err
	}
	ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	// No errors, container started
	return nil
}

// ContainerInfo gathers information on a container
func (c *Connection) ContainerInfo(id string) ([]byte, error) {
	url := fmt.Sprintf("%s/v1.0.0/libpod/containers/%s/json", c.baseurl, id)
	resp, err := c.netclient.Get(url)
	if err != nil {
		return make([]byte, 0), err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return make([]byte, 0), err
	}
	return body, nil
}

// ContainerLogsStdout retrieves a containers logs from stdout
func (c *Connection) ContainerLogsStdout(id string) ([]byte, error) {
	url := fmt.Sprintf("%s/v1.0.0/libpod/containers/%s/logs?stdout=true", c.baseurl, id)
	resp, err := c.netclient.Get(url)
	if err != nil {
		return make([]byte, 0), err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return make([]byte, 0), err
	}
	return body, nil
}

// ContainerLogsStderr retrieves a containers logs from stderr
func (c *Connection) ContainerLogsStderr(id string) ([]byte, error) {
	url := fmt.Sprintf("%s/v1.0.0/libpod/containers/%s/logs?stderr=true", c.baseurl, id)
	resp, err := c.netclient.Get(url)
	if err != nil {
		return make([]byte, 0), err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return make([]byte, 0), err
	}
	return body, nil
}

// NewConnection constructs a new Podman connection and connects to the provided SSH tunnel
func NewConnection(t sshtunnel.SSHTunnel) *Connection {
	c := &Connection{
		tunnel:    t,
		netclient: http.Client{Timeout: 20 * time.Second}, // TODO: configure timeout
	}
	log.Info().Msg("Starting tunnel")
	go c.tunnel.Start()
	time.Sleep(200 * time.Millisecond)
	c.baseurl = fmt.Sprintf("http://127.0.0.1:%d", c.tunnel.Local.Port)

	return c
}
