package azure

import (
	"context"
	"fmt"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/foundry499/api/pkg/rand"
	"gitlab.com/foundry499/api/pkg/types"

	"github.com/Azure/azure-sdk-for-go/services/compute/mgmt/2019-12-01/compute"
	"github.com/Azure/azure-sdk-for-go/services/network/mgmt/2019-12-01/network"
	"github.com/Azure/go-autorest/autorest/to"
)

// DeployVM provisions a new Virtual Machine on Azure
func (az *Azure) DeployVM(ctx context.Context, vm *types.VirtualMachine) (*types.VirtualMachine, error) {
	log.Info().Msgf("Started VM creation: %s", vm.Name)
	defer log.Info().Msgf("Finished VM creation: %s", vm.Name)

	timeout := time.Duration(az.Timeout) * time.Second

	// Prepare a timeout context
	log.Debug().Msgf("Set resource creation timeout: %d seconds", int(timeout.Seconds()))
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	// TODO: Choose a VM type based on the requested size
	log.Warn().Msg("VM size selection not implemented, defaulting to Standard_DS1_v2")
	vmType := compute.VirtualMachineSizeTypesStandardDS1V2
	vmDiskSize := int32(vm.Storage / 1024) // Convert MiB to GiB
	if vmDiskSize < 30 {                   // Minimum 30 GB disk mandated by Azure
		vmDiskSize = 30
	}

	// Create a Network Interface
	nic, ip, err := az.createNetworkInterface(ctx, vm.Name, az.VMNetwork, az.VMSubnet, az.VMSecurityGroup)
	if err != nil {
		log.Error().Err(err).Msgf("Failed to create network interface for VM: %s", vm.Name)
		return vm, err
	}

	// Create a Virtual Machines client
	c := compute.NewVirtualMachinesClient(az.SubscriptionID)
	c.Authorizer = az.getAuthorizer()

	// Configure the VM parameters
	azVM := compute.VirtualMachine{
		Location: to.StringPtr(az.Location),
		VirtualMachineProperties: &compute.VirtualMachineProperties{
			HardwareProfile: &compute.HardwareProfile{
				VMSize: vmType,
			},
			StorageProfile: &compute.StorageProfile{
				ImageReference: &compute.ImageReference{
					ID: to.StringPtr(az.VMWorkerImage),
				},
				OsDisk: &compute.OSDisk{
					CreateOption: compute.DiskCreateOptionTypesFromImage,
					DiskSizeGB:   to.Int32Ptr(vmDiskSize),
					ManagedDisk: &compute.ManagedDiskParameters{
						StorageAccountType: compute.StorageAccountTypesPremiumLRS, // Premium SSD, locally redundant (TODO: do we want fast or cheap disks? I chose fast.)
					},
					Name: to.StringPtr(fmt.Sprintf("%s-disk", vm.Name)),
				},
			},
			// Priority:       compute.Spot,
			// EvictionPolicy: compute.Deallocate,
			OsProfile: &compute.OSProfile{
				ComputerName: to.StringPtr(vm.Name),
				// Random admin credentials are generated but not saved - we will not use this admin account (but are required to create one).
				AdminUsername: to.StringPtr(rand.RandomHexString(8)),           // Random username
				AdminPassword: to.StringPtr("AbC4" + rand.RandomHexString(60)), // Random password with a prefix that satisfies the complexity requirements
				LinuxConfiguration: &compute.LinuxConfiguration{
					DisablePasswordAuthentication: to.BoolPtr(false),
				},
			},
			NetworkProfile: &compute.NetworkProfile{
				NetworkInterfaces: &[]compute.NetworkInterfaceReference{
					{
						ID: to.StringPtr(*nic.ID),
						NetworkInterfaceReferenceProperties: &compute.NetworkInterfaceReferenceProperties{
							Primary: to.BoolPtr(true),
						},
					},
				},
			},
		},
	}

	// Create a VM
	future, err := c.CreateOrUpdate(ctx, az.ResourceGroup, vm.Name, azVM)
	if err != nil {
		log.Error().Err(err).Msgf("Failed to create virtual machine %s, cleaning up", vm.Name)
		az.deleteNetworkInterface(ctx, vm.Name)
		az.deletePublicIPAddress(ctx, vm.Name)
		return vm, err
	}
	err = future.WaitForCompletionRef(ctx, c.Client)
	if err != nil {
		log.Error().Err(err).Msgf("Failed to create virtual machine %s, cleaning up", vm.Name)
		az.DeleteVM(ctx, vm.Name)
		az.deleteNetworkInterface(ctx, vm.Name)
		az.deletePublicIPAddress(ctx, vm.Name)
		return vm, err
	}
	_, err = future.Result(c)
	if err != nil {
		log.Error().Err(err).Msgf("Failed to create retrieve future result for VM %s, cleaning up", vm.Name)
		az.DeleteVM(ctx, vm.Name)
		az.deleteNetworkInterface(ctx, vm.Name)
		az.deletePublicIPAddress(ctx, vm.Name)
		return vm, err
	}
	log.Debug().Msgf("Created virtual machine: %s", vm.Name)

	// The VM is ready for jobs
	vm.IPAddress = types.NullString{}
	vm.IPAddress.String = *ip.IPAddress
	vm.IPAddress.Valid = true
	return vm, nil
}

func (az *Azure) createNetworkInterface(ctx context.Context, baseName string, vnetName string, subnetName string, securityGroup string) (network.Interface, network.PublicIPAddress, error) {
	name := fmt.Sprintf("%s-nic", baseName)

	// Get the existing subnet
	subnet, err := az.getSubnet(ctx, vnetName, subnetName)
	if err != nil {
		return network.Interface{}, network.PublicIPAddress{}, err
	}

	// Get an existing security group
	nsg, err := az.getSecurityGroup(ctx, securityGroup)
	if err != nil {
		return network.Interface{}, network.PublicIPAddress{}, err
	}

	// Create a public IP
	ip, err := az.createPublicIPAddress(ctx, baseName)
	if err != nil {
		return network.Interface{}, network.PublicIPAddress{}, err
	}

	// Prepare a network interface
	nic := network.Interface{
		Name:     to.StringPtr(name),
		Location: to.StringPtr(az.Location),
		InterfacePropertiesFormat: &network.InterfacePropertiesFormat{
			IPConfigurations: &[]network.InterfaceIPConfiguration{
				{
					Name: to.StringPtr(fmt.Sprintf("%s-ipconfig", baseName)),
					InterfaceIPConfigurationPropertiesFormat: &network.InterfaceIPConfigurationPropertiesFormat{
						Subnet:                    &subnet,
						PrivateIPAllocationMethod: network.Dynamic,
						PublicIPAddress:           &ip,
					},
				},
			},
			NetworkSecurityGroup: &nsg,
		},
	}

	// Prepare a Network Interface client
	c := network.NewInterfacesClient(az.SubscriptionID)
	c.Authorizer = az.getAuthorizer()

	// Create the network interface
	future, err := c.CreateOrUpdate(ctx, az.ResourceGroup, name, nic)
	if err != nil {
		log.Error().Msgf("Failed to create network interface: %v", err)
		az.deletePublicIPAddress(ctx, baseName)
		return network.Interface{}, ip, err
	}
	err = future.WaitForCompletionRef(ctx, c.Client)
	if err != nil {
		log.Error().Msgf("Failed to create network interface: %v", err)
		az.deletePublicIPAddress(ctx, baseName)
		return network.Interface{}, ip, err
	}

	log.Debug().Msgf("Created network interface: %s", name)
	res, err := future.Result(c)
	return res, ip, err
}

func (az *Azure) createPublicIPAddress(ctx context.Context, baseName string) (network.PublicIPAddress, error) {
	name := fmt.Sprintf("%s-ip", baseName)

	// Prepare a public IP address client
	c := network.NewPublicIPAddressesClient(az.SubscriptionID)
	c.Authorizer = az.getAuthorizer()

	// Create the public IP address
	future, err := c.CreateOrUpdate(
		ctx,
		az.ResourceGroup,
		name,
		network.PublicIPAddress{
			Name:     to.StringPtr(name),
			Location: to.StringPtr(az.Location),
			PublicIPAddressPropertiesFormat: &network.PublicIPAddressPropertiesFormat{
				PublicIPAddressVersion:   network.IPv4,
				PublicIPAllocationMethod: network.Static,
			},
		},
	)
	if err != nil {
		log.Error().Msgf("Failed to create public IP address: %v", err)
		return network.PublicIPAddress{}, err
	}
	err = future.WaitForCompletionRef(ctx, c.Client)
	if err != nil {
		log.Error().Msgf("Failed to create public IP address: %v", err)
		return network.PublicIPAddress{}, err
	}

	log.Debug().Msgf("Created public IP address: %s", name)
	return future.Result(c)
}

func (az *Azure) getSecurityGroup(ctx context.Context, name string) (network.SecurityGroup, error) {
	// Prepare a security group client
	client := network.NewSecurityGroupsClient(az.SubscriptionID)
	client.Authorizer = az.getAuthorizer()

	// Get the security group
	securityGroup, err := client.Get(ctx, az.ResourceGroup, name, "")
	if err != nil {
		log.Error().Msgf("Failed to retrieve network security group: %v", err)
		return network.SecurityGroup{}, err
	}

	log.Debug().Msgf("Retrieved security group: %s", name)
	return securityGroup, nil
}

func (az *Azure) getSubnet(ctx context.Context, vnetName string, subnetName string) (network.Subnet, error) {
	// Prepare a subnet client
	c := network.NewSubnetsClient(az.SubscriptionID)
	c.Authorizer = az.getAuthorizer()

	// Get the subnet
	subnet, err := c.Get(ctx, az.ResourceGroup, vnetName, subnetName, "")
	if err != nil {
		log.Error().Msgf("Failed to retrieve subnet: %v", err)
		return network.Subnet{}, err
	}

	log.Debug().Msgf("Retrieved subnet: %s/%s", vnetName, subnetName)
	return subnet, nil
}
