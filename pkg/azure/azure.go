package azure

// Azure holds the settings required to manage Azure resources.
// This should be populated from environment variables using envconfig.
type Azure struct {
	Timeout         int    `envconfig:"AZURE_TIMEOUT" default:"120"`
	SubscriptionID  string `envconfig:"AZURE_SUBSCRIPTION_ID" required:"true"`
	ResourceGroup   string `envconfig:"AZURE_RESOURCE_GROUP" required:"true"`
	Location        string `envconfig:"AZURE_LOCATION" default:"centralus"`
	VMWorkerImage   string `envconfig:"AZURE_WORKER_IMAGE" required:"true"`
	VMNetwork       string `envconfig:"AZURE_WORKER_NETWORK" required:"true"`
	VMSubnet        string `envconfig:"AZURE_WORKER_SUBNET" required:"true"`
	VMSecurityGroup string `envconfig:"AZURE_WORKER_SECURITY_GROUP" required:"true"`

	// These settings are read from envvars by azure.getAuthorizer(), but that function
	// doesn't run until a VM is deployed. These are included here to cause the Service
	// Queue to fail to start when they're missing (rather than panicking later).
	// Values can be obtained with `az ad sp create-for-rbac --sdk-auth`
	AuthTenantID     string `envconfig:"AZURE_TENANT_ID" required:"true"`
	AuthClientID     string `envconfig:"AZURE_CLIENT_ID" required:"true"`
	AuthClientSecret string `envconfig:"AZURE_CLIENT_SECRET" required:"true"`
}
