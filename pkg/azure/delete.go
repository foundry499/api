package azure

import (
	"context"
	"fmt"
	"time"

	"github.com/rs/zerolog/log"

	"github.com/Azure/azure-sdk-for-go/services/compute/mgmt/2019-12-01/compute"
	"github.com/Azure/azure-sdk-for-go/services/network/mgmt/2019-12-01/network"
	"github.com/Azure/go-autorest/autorest/to"
)

// DeleteVM deletes a Virtual Machine and its associated resources from Azure
func (az *Azure) DeleteVM(ctx context.Context, name string) error {
	log.Info().Msgf("Started VM deletion: %s", name)
	defer log.Info().Msgf("Finished VM deletion: %s", name)

	timeout := time.Duration(az.Timeout) * time.Second

	// Prepare a timeout context
	log.Debug().Msgf("Set resource deletion timeout: %d seconds", int(timeout.Seconds()))
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	// Prepare a Virtual Machines client
	c := compute.NewVirtualMachinesClient(az.SubscriptionID)
	c.Authorizer = az.getAuthorizer()

	// Power off the VM
	c.PowerOff(ctx, az.ResourceGroup, name, to.BoolPtr(true)) // true skips shutdown (force power off)
	log.Debug().Msgf("Powered off virtual machine: %s", name)

	// Delete the VM
	future, err := c.Delete(ctx, az.ResourceGroup, name)
	if err != nil {
		log.Error().Msgf("Failed to delete virtual machine: %v", err)
		return err
	}
	err = future.WaitForCompletionRef(ctx, c.Client)
	if err != nil {
		log.Error().Msgf("Failed to get virtual machine future response: %v", err)
		return err
	}
	log.Debug().Msgf("Deleted virtual machine: %s", name)

	// TODO: Everything after this can probably be parallelized with a WaitGroup

	// Delete the network interface
	err = az.deleteNetworkInterface(ctx, name)
	if err != nil {
		return err
	}

	// Delete the public IP address
	err = az.deletePublicIPAddress(ctx, name)
	if err != nil {
		return err
	}

	// Delete the disk
	err = az.deleteDisk(ctx, name)
	if err != nil {
		return err
	}

	return nil
}

func (az *Azure) deleteDisk(ctx context.Context, baseName string) error {
	name := fmt.Sprintf("%s-disk", baseName)

	// Prepare a Disk client
	c := compute.NewDisksClient(az.SubscriptionID)
	c.Authorizer = az.getAuthorizer()

	// Delete the OS disk
	future, err := c.Delete(ctx, az.ResourceGroup, name)
	if err != nil {
		log.Error().Msgf("Failed to delete disk: %v", err)
		return err
	}
	err = future.WaitForCompletionRef(ctx, c.Client)
	if err != nil {
		log.Error().Msgf("Failed to delete disk: %v", err)
		return err
	}

	log.Debug().Msgf("Deleted disk: %s", name)
	return nil
}

func (az *Azure) deleteNetworkInterface(ctx context.Context, baseName string) error {
	name := fmt.Sprintf("%s-nic", baseName)

	// Prepare a Network Interface client
	c := network.NewInterfacesClient(az.SubscriptionID)
	c.Authorizer = az.getAuthorizer()

	// Delete the network interface
	future, err := c.Delete(ctx, az.ResourceGroup, name)
	if err != nil {
		log.Error().Msgf("Failed to delete network interface: %v", err)
		return err
	}
	err = future.WaitForCompletionRef(ctx, c.Client)
	if err != nil {
		log.Error().Msgf("Failed to delete network interface: %v", err)
		return err
	}

	log.Debug().Msgf("Deleted network interface: %s", name)
	return nil
}

func (az *Azure) deletePublicIPAddress(ctx context.Context, baseName string) error {
	name := fmt.Sprintf("%s-ip", baseName)

	// Prepare a public IP address client
	c := network.NewPublicIPAddressesClient(az.SubscriptionID)
	c.Authorizer = az.getAuthorizer()

	// Delete the public IP address
	future, err := c.Delete(ctx, az.ResourceGroup, name)
	if err != nil {
		log.Error().Msgf("Failed to delete public IP address: %v", err)
		return err
	}
	err = future.WaitForCompletionRef(ctx, c.Client)
	if err != nil {
		log.Error().Msgf("Failed to delete public IP address: %v", err)
		return err
	}

	log.Debug().Msgf("Deleted public IP address: %s", name)
	return nil
}
