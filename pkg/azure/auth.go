package azure

import (
	"github.com/Azure/go-autorest/autorest"
	"github.com/Azure/go-autorest/autorest/azure/auth"
	"github.com/rs/zerolog/log"
)

func (az *Azure) getAuthorizer() autorest.Authorizer {
	// Create an authorizer from environment variables
	// Requires environment variables: AZURE_TENANT_ID, AZURE_CLIENT_ID, AZURE_CLIENT_SECRET
	// These can be obtained with `az ad sp create-for-rbac --sdk-auth`
	authorizer, err := auth.NewAuthorizerFromEnvironment()
	if err != nil {
		log.Panic().Err(err).Msg("Azure authorizer could not be created")
	}
	return authorizer
}
